import { Injectable, Inject } from '@nestjs/common';
import Web3 from 'web3';
import { UsersService } from '../users/users.service';
import { TasksService } from '../tasks/tasks.service';
import { ProjectsService } from '../projects/projects.service';
import { ProjectStatuses } from '../projects/projectStatuses.enum';
import { CommunitiesService } from '../communities/communities.service';

@Injectable()
export class EventsService {
  constructor(
    @Inject('Web3') private readonly web3: Web3,
    private usersService: UsersService,
    private tasksService: TasksService,
    private projectsService: ProjectsService,
    private communitiesService: CommunitiesService,
  ) {}

  async handleEventConfirmation(event: any) {
    switch (event.event) {
      case 'TaskCreated':
        await this.tasksService.escrow(event.returnValues['1']);
        console.log(event.event); // tslint:disable-line
        break;
      case 'ProjectCreated':
        console.log(event.event); // tslint:disable-line
        break;
      case 'NewSmartToken':
        console.log(event.event); // tslint:disable-line
        break;
      case 'Issuance':
        console.log(event.event); // tslint:disable-line
        break;
      case 'Destruction':
        console.log(event.event); // tslint:disable-line
        break;
      case 'Transfer':
        console.log(event.event); // tslint:disable-line
        break;
      case 'Approval':
        console.log(event.event); // tslint:disable-line
        break;
      case 'NewCommunityAddress':
        console.log(event.event); // tslint:disable-line
        break;
      case 'GenericLog':
        const genericMessage = event.returnValues['2'];
        switch (event.returnValues['1']) {
          case 'transferCurator':
            console.log('transferCurator'); // tslint:disable-line
            break;
          case 'transferVoteController':
            console.log('transferVoteController'); // tslint:disable-line
            break;
          case 'setMinimumStakingRequirement':
            console.log('setMinimumStakingRequirement'); // tslint:disable-line
            break;
          case 'setLockupPeriodSeconds':
            console.log('setLockupPeriodSeconds'); // tslint:disable-line
            break;
          case 'setLogger':
            console.log('setLogger'); // tslint:disable-line
            break;
          case 'setTokenAddresses':
            console.log('setTokenAddresses'); // tslint:disable-line
            break;
          case 'setCommunityAccount':
            console.log('setCommunityAccount'); // tslint:disable-line
            break;
          case 'setCommunityAccountOwner':
            console.log('setCommunityAccountOwner'); // tslint:disable-line
            break;
          case 'createNewTask':
            console.log('createNewTask'); // tslint:disable-line
            break;
          case 'cancelTask':
            this.tasksService.cancel(genericMessage);
            break;
          case 'rewardTaskCompletion':
            this.tasksService.complete(genericMessage);
            break;
          case 'createNewProject':
            this.projectsService.updateStatusByContractId(
              genericMessage,
              ProjectStatuses.Escrowed,
            );
            break;
          case 'cancelProject':
            this.projectsService.updateStatusByContractId(
              genericMessage,
              ProjectStatuses.Canceled,
            );
            break;
          case 'rewardProjectCompletion':
            this.projectsService.updateStatusByContractId(
              genericMessage,
              ProjectStatuses.Funded,
            );
            break;
          case 'unstakeCommunityTokens':
            const communityToUnstake = await this.communitiesService.findOneByAddress(
              event.returnValues[0],
            );
            const txToUnstake = await this.web3.eth.getTransaction(
              event.transactionHash,
            );
            if (!txToUnstake) {
              throw new Error('Transaction not found');
            }
            await this.usersService.unstake(
              txToUnstake.from,
              communityToUnstake,
            );
            break;
          case 'stakeCommunityTokens':
            const communityToStake = await this.communitiesService.findOneByAddress(
              event.returnValues[0],
            );
            const txToStake = await this.web3.eth.getTransaction(
              event.transactionHash,
            );
            if (!txToStake) {
              throw new Error('Transaction not found');
            }
            await this.usersService.stake(txToStake.from, communityToStake);
            break;
        }
        break;
      case 'OwnerUpdate':
        console.log(event.event); // tslint:disable-line
        break;
    }
  }

  async handleEventHead(event: any) {
    switch (event.event) {
      case 'GenericLog':
        switch (event.returnValues['1']) {
          case 'stakeCommunityTokens':
            const txToStake = await this.web3.eth.getTransaction(
              event.transactionHash,
            );
            if (!txToStake) {
              throw new Error('Transaction not found');
            }
            await this.usersService.preStake(txToStake.from);
            break;
          case 'unstakeCommunityTokens':
            const tx = await this.web3.eth.getTransaction(
              event.transactionHash,
            );
            if (!tx) {
              throw new Error('Transaction not found');
            }
            await this.usersService.setStatusToPending(tx.from);
            break;
        }
    }
  }
}
