import { Module } from '@nestjs/common';
import { web3Providers } from '../blockchain/web3.providers';
import { UsersModule } from '../users/users.module';
import { CommunitiesModule } from '../communities/communities.module';
import { EventsService } from './events.service';
import { TasksModule } from '../tasks/tasks.module';
import { ProjectsModule } from '../projects/projects.module';

@Module({
  imports: [UsersModule, TasksModule, ProjectsModule, CommunitiesModule],
  controllers: [],
  providers: [EventsService, ...web3Providers],
  exports: [EventsService],
})
export class EventsModule {}
