import { Test, TestingModule } from '@nestjs/testing';
import { PollsController } from './polls.controller';
import { PollsService } from './polls.service';
import { VotesService } from '../votes/votes.service';
import { CommunitiesService } from '../communities/communities.service';
import { ProjectsService } from '../projects/projects.service';
import { OptionsService } from '../polls/options/options.service';
import { MailerService } from '../mailer/mailer.service';
import { UsersService } from '../users/users.service';
import { SessionsService } from '../session/sessions.service';
import { MessagesService } from '../messages/messages.service';
import { createConnection } from 'typeorm';
import { Poll } from './poll.entity';
import { Community } from '../communities/community.entity';
import { Project } from '../projects/project.entity';
import { Option } from '../polls/options/option.entity';
import { Session } from '../session/session.entity';
import { Message } from '../messages/message.entity';
import { Vote } from '../votes/vote.entity';
import { User } from '../users/user.entity';
import { Statuses } from '../users/statuses.enum';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';

import { getRepositoryToken } from '@nestjs/typeorm';
import httpMocks from 'node-mocks-http';

import {
  newPoll,
  newOption1,
  newOption2,
  newOption3,
  newCommunity,
  newUser,
  newUser2,
  newUser3,
  newUser4,
  newUser5,
  newUser6,
  getSession,
  existingCurator,
  newProject,
} from '../../common/helpers/test/fixtures';
import { S3uploaderService } from '../s3uploader/s3uploader.service';
import { ApprovalVotes } from './approval.votes.enum';
const request = httpMocks.createRequest({
  method: 'POST',
  url: '/polls',
});
const session = getSession(request, existingCurator.user);

describe('PollsController', () => {
  let pollsModule: TestingModule;
  let pollsController: PollsController;
  let pollsService: PollsService;
  let communitiesService: CommunitiesService;
  let projectsService: ProjectsService;
  let optionsService: OptionsService;
  let usersService: UsersService;
  let s3uploaderService: S3uploaderService;

  beforeAll(async () => {
    const testConnection = await createConnection({
      type: 'sqljs',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      logging: false,
      dropSchema: true,
      synchronize: true,
    });
    const communityUserStatusRepository = testConnection.getRepository(
      CommunityUserStatus,
    );
    const pollsRepository = testConnection.getRepository(Poll);
    const optionsRepository = testConnection.getRepository(Option);
    const votesRepository = testConnection.getRepository(Vote);
    const communitiesRepository = testConnection.getRepository(Community);
    const projectsRepository = testConnection.getRepository(Project);
    const usersRepository = testConnection.getRepository(User);
    const sessionsRepository = testConnection.getRepository(Session);
    const messagesRepository = testConnection.getRepository(Message);

    pollsModule = await Test.createTestingModule({
      controllers: [PollsController],
      providers: [
        {
          provide: getRepositoryToken(CommunityUserStatus),
          useValue: communityUserStatusRepository,
        },
        CommunitiesService,
        {
          provide: getRepositoryToken(Community),
          useValue: communitiesRepository,
        },
        ProjectsService,
        {
          provide: getRepositoryToken(Project),
          useValue: projectsRepository,
        },
        MailerService,
        PollsService,
        {
          provide: getRepositoryToken(Poll),
          useValue: pollsRepository,
        },
        OptionsService,
        {
          provide: getRepositoryToken(Option),
          useValue: optionsRepository,
        },
        VotesService,
        {
          provide: getRepositoryToken(Vote),
          useValue: votesRepository,
        },
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        SessionsService,
        {
          provide: getRepositoryToken(Session),
          useValue: sessionsRepository,
        },
        MessagesService,
        {
          provide: getRepositoryToken(Message),
          useValue: messagesRepository,
        },
        S3uploaderService,
      ],
    }).compile();

    pollsController = pollsModule.get<PollsController>(PollsController);
    pollsService = pollsModule.get<PollsService>(PollsService);
    optionsService = pollsModule.get<OptionsService>(OptionsService);
    communitiesService = pollsModule.get<CommunitiesService>(
      CommunitiesService,
    );
    projectsService = pollsModule.get<ProjectsService>(ProjectsService);
    usersService = pollsModule.get<UsersService>(UsersService);
    s3uploaderService = pollsModule.get<S3uploaderService>(S3uploaderService);
  });

  describe('communities', () => {
    let user;
    let community;

    beforeAll(async () => {
      const savedCommunity = await communitiesService.createAndSave(
        newCommunity,
      );
      community = await communitiesService.findOne(savedCommunity.id);
      await usersService.createAndSave(newUser);
      user = await usersService.findOneByAddress(newUser.address);
      newPoll.options.push(newOption1, newOption2, newOption3);
      user.memberOf.push(community);
      user.curatorOf.push(community);
      await usersService.save(user);
      session.user = user;
    });

    it('should create and save a poll', async () => {
      const result = await pollsController.create(newPoll, session);
      expect(result.id).toBe(1);
    });

    it('should create and save a poll with a project association', async () => {
      // creating a project
      const project = await projectsService.createAndSave(newProject);
      // creating the poll
      newPoll.project = project;
      const result = await pollsController.create(newPoll, session);
      expect(result.title).toBe(newPoll.title);
      expect(result.project.title).toBe(project.title);
      const findResult = await pollsController.findOne(result.id, session);
      expect(findResult.title).toBe(newPoll.title);
      expect(findResult.project.title).toBe(project.title);

      const findByProject = await pollsService.findAllByProject(project.id);
      expect(findByProject.length).toBeGreaterThanOrEqual(1);
    });

    it('should get newly created poll', async () => {
      const poll = await pollsController.create(newPoll, session);
      const result = await pollsController.findOne(poll.id, session);
      expect(result.title).toBe(newPoll.title);
    });

    it('should get newly created poll with a user', async () => {
      const poll = await pollsController.create(newPoll, session);
      const firstHasVotedFalseResult = await pollsController.findOne(
        poll.id,
        session,
      );
      expect(firstHasVotedFalseResult.hasVoted).toBe(false);
      const voteBody = { optionId: poll.options[0].id };
      await pollsController.vote(poll.id, voteBody, {
        user,
      });
      const secondHasVotedResult = await pollsController.findOne(
        poll.id,
        session,
      );
      expect(secondHasVotedResult.hasVoted).toBe(true);
    });

    it('should allow voting', async () => {
      const poll = await pollsController.create(newPoll, session);
      const voteBody = { optionId: poll.options[0].id };
      const result = await pollsController.vote(poll.id, voteBody, {
        user,
      });
      expect(result.option).toBe(poll.options[0].id);
      expect(result.poll.question).toBe(poll.question);
    });

    it('should not allow users to vote twice', async () => {
      const poll = await pollsController.create(newPoll, session);
      const voteBody = { optionId: poll.options[0].id };
      await pollsController.vote(poll.id, voteBody, {
        user,
      });
      const samePoll = await pollsController.findOne(1, session);
      const newVoteBody = { optionId: poll.options[1].id };
      try {
        await pollsController.vote(samePoll.id, newVoteBody, {
          user,
        });
      } catch (error) {
        expect(error.message).toBe('You have already voted on this poll.');
      }
    });

    it('calculate vote percentage', async () => {
      const poll = await pollsController.create(newPoll, session);
      const approvalVote = { optionId: poll.options[0].id };
      const denyVote = { optionId: poll.options[1].id };
      await pollsController.vote(poll.id, approvalVote, {
        user,
      });
      user = await createUserForPoll(newUser2);
      await pollsController.vote(poll.id, denyVote, {
        user,
      });
      user = await createUserForPoll(newUser3);
      await pollsController.vote(poll.id, denyVote, {
        user,
      });

      let resultSet = { ...(await getResults(poll)) };
      expect(resultSet.supported).toBe(false);
      expect(resultSet.approvalOption.support).toBe(33.33);
      expect(resultSet.quorum).toBe(33.33);
      expect(resultSet.quorumMet).toBe(false);
      expect(resultSet.willPass).toBe(false);

      user = await createUserForPoll(newUser4);
      await pollsController.vote(poll.id, approvalVote, {
        user,
      });
      resultSet = { ...(await getResults(poll)) };
      expect(resultSet.supported).toBe(true);
      expect(resultSet.approvalOption.support).toBe(50.0);
      expect(resultSet.quorum).toBe(50.0);
      expect(resultSet.quorumMet).toBe(true);
      expect(resultSet.willPass).toBe(true);

      user = await createUserForPoll(newUser5);
      resultSet = { ...(await getResults(poll)) };
      expect(resultSet.supported).toBe(true);
      expect(resultSet.approvalOption.support).toBe(50.0);
      expect(resultSet.quorum).toBe(40.0);
      expect(resultSet.quorumMet).toBe(false);
      expect(resultSet.willPass).toBe(false);

      user = await createUserForPoll(newUser6);
      await pollsController.vote(poll.id, approvalVote, {
        user,
      });
      resultSet = { ...(await getResults(poll)) };
      expect(resultSet.supported).toBe(true);
      expect(resultSet.approvalOption.support).toBe(60.0);
      expect(resultSet.quorum).toBe(50.0);
      expect(resultSet.quorumMet).toBe(true);
      expect(resultSet.willPass).toBe(true);
    });

    it('should throw an exception if user is pending', async () => {
      user.curatorOf.push(community);
      await usersService.save(user);
      const result = await pollsController.create(newPoll, session);
      const email = 'foss@setFlagsFromString.com';
      const voteBody = { optionId: result.options[0].id };
      user.memberOf.push(result.community);
      user.status = Statuses.Pending;
      await usersService.save(user);
      try {
        await pollsController.vote(result.id, voteBody, {
          user,
        });
      } catch (error) {
        expect(error.message).toBe(
          'You cannot perform this action while a transaction is pending.',
        );
      }
    });

    it('should not allow voting if poll closed', async () => {
      newPoll.endDate = newPoll.startDate;
      user.status = null;
      await usersService.save(user);
      const poll = await pollsController.create(newPoll, session);
      const voteBody = { optionId: poll.options[0].id };
      try {
        await pollsController.vote(poll.id, voteBody, {
          user,
        });
      } catch (error) {
        expect(error.message).toBe('Poll is Closed');
      }
    });
    it('should not allow voting if poll closed', async () => {
      newPoll.endDate = newPoll.startDate;
      user.status = null;
      await usersService.save(user);
      const poll = await pollsController.create(newPoll, session);
      const voteBody = { optionId: poll.options[0].id };
      try {
        await pollsController.vote(poll.id, voteBody, {
          user,
        });
      } catch (error) {
        expect(error.message).toBe('Poll is Closed');
      }
    });

    async function createUserForPoll(newUserDto) {
      await usersService.createAndSave(newUserDto);
      user = await usersService.findOneByAddress(newUserDto.address);
      session.user = user;
      user.memberOf.push(community);
      await usersService.save(user);
      return user;
    }
    async function getResults(poll) {
      const foundPoll = await pollsController.findOne(poll.id, session);
      const pollResults = await pollsService.calculateResults(foundPoll);
      const supported = await pollsService.checkIfSupported(foundPoll);
      const approvalOption = pollResults.find((option) => {
        return option.name === ApprovalVotes.Approve;
      });
      community = await communitiesService.findOne(community.id);
      const quorum = await pollsService.calculateApprovalQuorum(
        foundPoll,
        community.memberCount,
      );
      const quorumMet = await pollsService.checkIfQuorumMet(
        foundPoll,
        community,
      );
      const willPass = await pollsService.checkIfWillPass(foundPoll, community);
      return {
        foundPoll,
        pollResults,
        supported,
        approvalOption,
        quorum,
        quorumMet,
        willPass,
      };
    }
  });
});
