import { Option } from './options/option.entity';

export class PollDto {
  communityId: number;
  projectId?: string;
  title: string;
  description: string;
  question: string;
  fileUrl: string;
  startDate: string;
  endDate: string;
  options: Array<any>;
  project?: any;
}
