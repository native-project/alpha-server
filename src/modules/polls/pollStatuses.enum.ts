export enum PollStatuses {
  Created = 'created',
  Canceled = 'canceled',
}
