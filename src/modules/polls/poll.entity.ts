import {
  Entity,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  JoinTable,
} from 'typeorm';
import {
  Length,
  IsDateString,
  ValidateNested,
  IsOptional,
  Validate,
} from 'class-validator';

import { Community } from '../communities/community.entity';
import { Project } from '../projects/project.entity';
import { Option } from './options/option.entity';
import { Vote } from '../votes/vote.entity';
import { ChoiceName } from '../../common/helpers/utils/choiceValidator';
import { PollStatuses } from './pollStatuses.enum';

@Entity()
export class Poll {
  @PrimaryGeneratedColumn() id: number;

  @Column()
  @Length(3, 128)
  title: string;

  @Column('text', { nullable: true, default: '' })
  @IsOptional()
  description: string;

  @Column()
  @Length(2, 256)
  question: string;

  @Column({ type: 'varchar', default: PollStatuses.Created })
  @Validate(ChoiceName, [PollStatuses])
  @IsOptional()
  status: PollStatuses;

  @Column({ nullable: true, default: null })
  @IsOptional()
  fileUrl: string;

  @Column()
  @IsDateString()
  startDate: Date;

  @Column()
  @IsDateString()
  endDate: Date;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;

  @ManyToOne((type) => Community, (community) => community.polls, {
    eager: true,
  })
  community: Community;

  @ManyToOne((type) => Project, (project) => project.polls, {
    eager: true,
  })
  project: Project;

  @OneToMany((type) => Option, (option) => option.poll)
  @JoinTable()
  @ValidateNested()
  options: Option[];

  @OneToMany((type) => Vote, (vote) => vote.poll)
  votes: Vote[];
}
