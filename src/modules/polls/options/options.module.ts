import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { optionsProviders } from './options.providers';
import { OptionsService } from './options.service';
import { Option } from './option.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Option])],
  providers: [OptionsService, ...optionsProviders],
  exports: [OptionsService],
})
export class OptionsModule {}
