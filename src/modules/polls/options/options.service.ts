import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Option } from './option.entity';
import { OptionDto } from './option.dto';
import { validate } from 'class-validator';

@Injectable()
export class OptionsService {
  constructor(
    @InjectRepository(Option)
    private readonly optionsRepository: Repository<Option>,
  ) {}

  async findAllByPoll(pollId): Promise<Option[]> {
    return await this.optionsRepository.find({
      where: {
        pollId,
      },
    });
  }

  async createAndSave(optionDto: OptionDto): Promise<Option> {
    const errors = await validate(optionDto);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const option: Option = this.create(optionDto);
    return await this.optionsRepository.save(option);
  }

  create(optionDto: OptionDto): Option {
    return this.optionsRepository.create(optionDto);
  }
}
