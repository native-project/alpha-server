import {
  Entity,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Length } from 'class-validator';
import { Poll } from '../poll.entity';
import { Vote } from '../../votes/vote.entity';

@Entity()
export class Option {
  @PrimaryGeneratedColumn() id: number;

  @Column()
  @Length(2, 128)
  name: string;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;

  @ManyToOne((type) => Poll, (poll) => poll.options, {
    eager: false,
  })
  poll: Poll;

  @OneToMany((type) => Vote, (vote) => vote.option, {
    eager: false,
  })
  votes: Vote[];
}
