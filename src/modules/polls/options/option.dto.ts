import { Poll } from '../poll.entity';

export class OptionDto {
  poll: Poll;
  name: string;
}
