import { Connection } from 'typeorm';
import { Option } from './option.entity';

export const optionsProviders = [
  {
    provide: 'OptionsRepository',
    useFactory: (connection: Connection) => connection.getRepository(Option),
    inject: [Connection],
  },
];
