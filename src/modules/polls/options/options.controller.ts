import { Controller, Get, Param } from '@nestjs/common';
import { OptionsService } from './options.service';

@Controller('options')
export class OptionsController {
  constructor(private readonly optionsService: OptionsService) {}

  @Get(':pollId')
  async findAllByPoll(@Param('pollId') pollId) {
    return await this.optionsService.findAllByPoll(pollId);
  }
}
