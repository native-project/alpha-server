import { Connection } from 'typeorm';
import { Poll } from './poll.entity';

export const pollsProviders = [
  {
    provide: 'PollsRepository',
    useFactory: (connection: Connection) => connection.getRepository(Poll),
    inject: [Connection],
  },
];
