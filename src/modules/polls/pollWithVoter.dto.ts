import { Poll } from './poll.entity';

export class PollWithVoterDto extends Poll {
  hasVoted: boolean;
}
