import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Session,
  UseGuards,
} from '@nestjs/common';
import { PollsService } from './polls.service';
import { PollDto } from './poll.dto';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { UsersService } from '../users/users.service';
import {
  permissionedCurator,
  permissionedMember,
} from '../users/permission.helpers';

@Controller('polls')
@UseGuards(RolesGuard)
export class PollsController {
  constructor(
    private readonly pollsService: PollsService,
    private readonly usersService: UsersService,
  ) {}

  @Get(':pollId')
  async findOne(@Param('pollId') pollId, @Session() session) {
    let poll;
    if (!session || !session.user) {
      return await this.pollsService.findOne(pollId);
    }
    const polls = [await this.pollsService.findOneWithVoters(pollId)];
    if (polls.length > 0) {
      const user = await this.usersService.findOneByAddress(
        session.user.address,
      );
      const pollsWithVoterInfo = await this.pollsService.addVoterInfo(
        polls,
        user,
      );
      poll = pollsWithVoterInfo[0];
    }
    return poll;
  }

  @Post(':pollId/vote')
  @Roles('curator', 'member')
  async vote(@Param('pollId') pollId, @Body() voteBody, @Session() session) {
    if (!voteBody.optionId || +voteBody.optionId <= 0) {
      throw new HttpException(
        'You must provide a valid vote option',
        HttpStatus.BAD_REQUEST,
      );
    }

    const poll = await this.pollsService.findOneWithVoters(pollId);
    if (!poll.community || !poll.community.id) {
      throw new HttpException(
        'This poll is not associated with a community.',
        HttpStatus.BAD_REQUEST,
      );
    }

    const sessionUser = await permissionedMember(
      session,
      poll.community.id,
      this.usersService,
    );

    const votedPreviously = await this.pollsService.checkPreviouslyVoted(
      poll,
      sessionUser,
    );
    if (votedPreviously) {
      throw new HttpException(
        'You have already voted on this poll.',
        HttpStatus.UNAUTHORIZED,
      );
    }

    return this.pollsService.createAndSaveVote(sessionUser, voteBody, poll);
  }

  @Post()
  @Roles('curator', 'admin')
  async create(@Body() poll: PollDto, @Session() session) {
    await permissionedCurator(session, poll.communityId, this.usersService);

    return await this.pollsService.createAndSave(poll);
  }
}
