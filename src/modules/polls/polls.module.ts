import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { pollsProviders } from './polls.providers';
import { communitiesProviders } from '../communities/communities.providers';
import { projectsProviders } from '../projects/projects.providers';
import { optionsProviders } from './options/options.providers';
import { PollsService } from './polls.service';
import { PollsController } from './polls.controller';
import { Poll } from './poll.entity';
import { OptionsModule } from './options/options.module';
import { Community } from '../communities/community.entity';
import { Project } from '../projects/project.entity';
import { Option } from './options/option.entity';
import { Vote } from '../votes/vote.entity';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Poll]),
    TypeOrmModule.forFeature([Community]),
    TypeOrmModule.forFeature([Project]),
    TypeOrmModule.forFeature([Option]),
    TypeOrmModule.forFeature([Vote]),
    OptionsModule,
    UsersModule,
  ],
  controllers: [PollsController],
  providers: [
    PollsService,
    ...communitiesProviders,
    ...projectsProviders,
    ...pollsProviders,
    ...optionsProviders,
  ],
  exports: [PollsService],
})
export class PollsModule {}
