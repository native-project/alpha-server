import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { validate } from 'class-validator';

import { Poll } from './poll.entity';
import { PollStatuses } from './pollStatuses.enum';
import { Community } from '../communities/community.entity';
import { Project } from '../projects/project.entity';
import { PollDto } from './poll.dto';
import { Option } from './options/option.entity';
import { Vote } from '../votes/vote.entity';
import { ApprovalVotes } from './approval.votes.enum';
import { isDate } from 'util';

@Injectable()
export class PollsService {
  constructor(
    @InjectRepository(Poll) private readonly pollsRepository: Repository<Poll>,
    @InjectRepository(Community)
    private readonly communitiesRepository: Repository<Community>,
    @InjectRepository(Project)
    private readonly projectsRepository: Repository<Project>,
    @InjectRepository(Option)
    private readonly optionsRepository: Repository<Option>,
    @InjectRepository(Vote) private readonly votesRepository: Repository<Vote>,
  ) {}
  async findOne(id): Promise<Poll> {
    return await this.pollsRepository.findOne(id, {
      relations: ['options', 'options.votes', 'votes'],
    });
  }

  async findOneWithVoters(id): Promise<Poll> {
    return await this.pollsRepository.findOne(id, {
      relations: ['options', 'options.votes', 'votes', 'votes.user'],
    });
  }

  async findAllByCommunity(communityId): Promise<Poll[]> {
    const community: Community = await this.communitiesRepository.findOne(
      communityId,
    );

    return await this.pollsRepository.find({
      where: {
        community,
        project: null,
      },
      relations: ['options', 'options.votes', 'votes'],
    });
  }

  async findAllByCommunityWithVoters(communityId): Promise<Poll[]> {
    const community: Community = await this.communitiesRepository.findOne(
      communityId,
    );

    return await this.pollsRepository.find({
      where: {
        community,
        project: null,
      },
      relations: ['options', 'options.votes', 'votes', 'votes.user'],
    });
  }

  async findAllByProject(projectId): Promise<Poll[]> {
    const project: Project = await this.projectsRepository.findOne(projectId);

    return await this.pollsRepository.find({
      where: {
        project,
      },
      relations: ['options', 'options.votes', 'votes'],
    });
  }

  async createAndSave(pollDto: PollDto): Promise<Poll> {
    const poll: Poll = this.create(pollDto);
    const errors = await validate(poll);
    const community: Community = await this.communitiesRepository.findOne(
      pollDto.communityId,
    );

    if (community === undefined) {
      throw new HttpException('Invalid community', HttpStatus.BAD_REQUEST);
    }

    if (pollDto.projectId !== undefined) {
      const project: Project = await this.projectsRepository.findOne(
        pollDto.projectId,
      );

      if (project === undefined) {
        throw new HttpException('Invalid project', HttpStatus.BAD_REQUEST);
      } else {
        poll.project = project;
      }
    }

    poll.community = community;
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }

    const newPoll = await this.pollsRepository.save(poll);
    await this.createAndSaveOptions(pollDto.options, newPoll);
    return await this.pollsRepository.findOne(newPoll.id, {
      relations: ['options', 'options.votes', 'votes'],
    });
  }

  create(pollDto: PollDto): Poll {
    return this.pollsRepository.create(pollDto);
  }

  async createAndSaveForProject(project: Project): Promise<Poll> {
    const poll = {
      projectId: project.id,
      title: project.title,
      description: project.description,
      question: `Approval of ${project.title}`,
      fileUrl: null,
      startDate: isDate(project.startDate)
        ? project.startDate.toISOString()
        : project.startDate,
      endDate: isDate(project.endDate)
        ? project.endDate.toISOString()
        : project.endDate,
      options: [{ name: 'Approve' }, { name: 'Deny' }],
      communityId: project.community.id,
      project,
    };
    const createdPoll = await this.createAndSave(poll);
    if (!createdPoll) {
      throw new HttpException(
        'Poll unable to be created.',
        HttpStatus.BAD_REQUEST,
      );
    }
    return createdPoll;
  }

  async createAndSaveVote(user, voteBody, poll): Promise<Vote> {
    const isClosed = await this.checkIfPollClosed(poll.id);
    if (isClosed) {
      throw new HttpException('Poll is Closed', HttpStatus.FORBIDDEN);
    }
    const voteDto = {
      user,
      option: voteBody.optionId,
      poll,
    };
    const vote: Vote = this.votesRepository.create(voteDto);
    await this.votesRepository.save(vote);
    return vote;
  }

  async checkPreviouslyVoted(poll, user) {
    const votedPreviously = poll.votes.find((voteRecord) => {
      return !voteRecord.user ? false : voteRecord.user.id === user.id;
    });
    return votedPreviously ? true : false;
  }

  async checkIfPollClosed(pollId) {
    const poll = await this.pollsRepository.findOne(pollId);
    const date = new Date();
    const isClosed =
      new Date(poll.endDate) < date || new Date(poll.startDate) > date;
    return isClosed;
  }

  async checkIfWillPass(poll, community: Community = null): Promise<boolean> {
    community = !community
      ? await this.communitiesRepository.findOne(poll.community.id)
      : community;
    const supported = await this.checkIfSupported(poll, community);
    const quorumMet = await this.checkIfQuorumMet(poll, community);
    return supported && quorumMet;
  }

  async checkIfSupported(poll, community: Community = null): Promise<boolean> {
    community = !community
      ? await this.communitiesRepository.findOne(poll.community.id)
      : community;
    const supportedArray = await this.calculateResults(poll);
    let support = 0;
    supportedArray.map((option) => {
      if (option.name === ApprovalVotes.Approve) {
        support = option.support;
      }
    });
    return support >= community.support;
  }

  async checkIfQuorumMet(poll, community: Community = null): Promise<boolean> {
    community = !community
      ? await this.communitiesRepository.findOne(poll.community.id)
      : community;
    const quorum = await this.calculateApprovalQuorum(
      poll,
      community.memberCount,
    );
    return quorum >= community.quorum;
  }

  async calculateResults(poll) {
    const results = [];
    let totalVoteCount = 0;
    poll.options.forEach((option) => {
      results.push({ name: option.name, voteCount: option.votes.length });
      totalVoteCount += option.votes.length;
    });
    results.forEach((option) => {
      option.support = +((option.voteCount / totalVoteCount) * 100).toFixed(2);
    });
    return results;
  }

  async calculateApprovalQuorum(poll, memberCount): Promise<number> {
    if (memberCount === 0) {
      return 0;
    }
    let approvalVotes = 0;
    poll.options.forEach((option) => {
      if (option.name === ApprovalVotes.Approve) {
        approvalVotes += option.votes.length;
      }
    });
    return +((approvalVotes / memberCount) * 100).toFixed(2);
  }

  async addVoterInfo(polls, user) {
    const pollsWithVoter = [];
    polls.map(async (poll) => {
      const pollWithVoter = { ...poll, hasVoted: false };
      pollWithVoter.hasVoted = await this.checkPreviouslyVoted(poll, user);
      pollsWithVoter.push(pollWithVoter);
    });
    return pollsWithVoter;
  }

  async cancel(pollId): Promise<boolean> {
    try {
      const poll = await this.pollsRepository.findOne(pollId);
      poll.status = PollStatuses.Canceled;
      await this.pollsRepository.save(poll);
      return true;
    } catch (e) {
      return false;
    }
  }

  async createAndSaveOptions(options, poll) {
    for (const item of options) {
      const option = this.optionsRepository.create({
        name: item.name,
        poll,
      });
      await this.optionsRepository.save(option);
    }
  }

  async hasVotes(poll: Poll): Promise<boolean> {
    if (!poll.votes) {
      poll = await this.findOneWithVoters(poll.id);
    }
    return poll.votes && poll.votes.length > 0;
  }
}
