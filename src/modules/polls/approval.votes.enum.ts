export enum ApprovalVotes {
  Approve = 'yes',
  Deny = 'no',
}
