import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { HttpStrategy } from './http.strategy';
import { AuthGuard } from './auth.guard';
@Module({
  providers: [AuthService, HttpStrategy, AuthGuard],
})
export class AuthModule {}
