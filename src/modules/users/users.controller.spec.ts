import { Test, TestingModule } from '@nestjs/testing';
import { createConnection } from 'typeorm';
import { UsersController } from './users.controller';
import httpMocks from 'node-mocks-http';
import { UsersService } from './users.service';
import { SessionsService } from '../session/sessions.service';
import { MessagesService } from '../messages/messages.service';
import { MailerService } from '../mailer/mailer.service';
import { Message } from '../messages/message.entity';
import { User } from './user.entity';
import { Session } from '../session/session.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import {
  newUser,
  existingUser,
  getSession,
} from '../../common/helpers/test/fixtures';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';
import { communityUserStatus } from '../communityUserStatus/communityUserStatus.enum';

describe('UsersController', () => {
  let usersModule: TestingModule;
  let usersController: UsersController;
  let usersService: UsersService;

  const request = httpMocks.createRequest({
    method: 'GET',
    url: '/user',
  });
  const session = getSession(request);

  beforeAll(async () => {
    const testConnection = await createConnection({
      type: 'sqljs',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      logging: false,
      dropSchema: true,
      synchronize: true,
    });
    const usersRepository = testConnection.getRepository(User);
    const sessionsRepository = testConnection.getRepository(Session);
    const messagesRepository = testConnection.getRepository(Message);
    const communityUserStatusRepository = testConnection.getRepository(
      CommunityUserStatus,
    );

    usersModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        MailerService,
        SessionsService,
        {
          provide: getRepositoryToken(Session),
          useValue: sessionsRepository,
        },
        MessagesService,
        {
          provide: getRepositoryToken(Message),
          useValue: messagesRepository,
        },
        {
          provide: getRepositoryToken(CommunityUserStatus),
          useValue: communityUserStatusRepository,
        },
      ],
    }).compile();

    usersController = usersModule.get<UsersController>(UsersController);
    usersService = usersModule.get<UsersService>(UsersService);
  });

  describe('users', () => {
    it('should get an existing user', async () => {
      jest
        .spyOn(usersService, 'findOneByAddress')
        .mockImplementation(() => existingUser.user);
      const result = await usersController.getUser(request, session);
      expect(result.user).toBe(existingUser.user);
    });

    it('should return an array of messages with a user', async () => {
      jest
        .spyOn(usersService, 'findOneByAddress')
        .mockImplementation(() => existingUser.user);
      const result = await usersController.getUser(request, session);
      expect(result.user.messages);
    });

    it('should return an empty response if no user in session', async () => {
      delete session.user;
      const result = await usersController.getUser(request, session);
      expect(result.user).toBe('');
    });
  });

  describe('signing', () => {
    it('should return message to sign', async () => {
      const message = `Terms and Conditions for user ${newUser.address}`;
      jest
        .spyOn(usersService, 'getMessageToSign')
        .mockImplementation(() => message);
      const receivedMessage = await usersController.getMessageToSign(newUser);
      expect(receivedMessage).toBe(message);
    });

    it('should throw an exception if no address is provided', async () => {
      usersController.getMessageToSign(newUser).catch((e) => {
        expect(e.Error).toBe('You must supply an address.');
      });
    });
  });

  describe('authorize', () => {
    it('should return user if authorized', async (done) => {
      jest
        .spyOn(usersService, 'authenticateUser')
        .mockImplementation(() => existingUser.user);
      const userResponse = await usersController.authenticate(
        { address: newUser.address, signature: 'xxxxxxxxxxxx' },
        session,
      );
      if (userResponse) {
        expect(userResponse.id).toBe(existingUser.user.id);
      } else {
        done.fail(new Error('Empty response.'));
      }
      done();
    });

    it('should return empty if not authorized', async (done) => {
      jest.spyOn(usersService, 'authenticateUser').mockImplementation(() => '');
      const userResponse = await usersController.authenticate(
        { address: newUser.address, signature: 'xxxxxxxxxxxx' },
        session,
      );
      if (userResponse === '') {
        expect(true);
      } else {
        done.fail(new Error('Non-empty'));
      }
      done();
    });

    it('should throw an exception if no address is provided', async () => {
      usersController
        .authenticate(
          { signature: 'NhY8rZ0rAsibnpLaaxUNjdNUFoHHkEnJ' },
          session,
        )
        .catch((e) => {
          expect(e.message).toBe('You must supply an address and signature.');
        });
    });

    it('should throw an exception if no signature is provided', async () => {
      usersController.authenticate(newUser, session).catch((e) => {
        expect(e.message).toBe('You must supply an address and signature.');
      });
    });
  });

  describe('authorize', () => {
    it('should return existing user', async () => {
      jest
        .spyOn(usersService, 'findOneByAddress')
        .mockImplementation(() => existingUser.user);
      const existingUserResponse = await usersController.findOne(
        { address: existingUser.user.address },
        session,
      );
      expect(existingUserResponse.id).toBe(existingUser.user.id);
    });
  });
});
