import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { Session } from '../session/session.entity';
import { Message } from '../messages/message.entity';
import { usersProviders } from './users.providers';
import { appProviders } from '../../app.providers';
import { communityUserStatusProviders } from '../communityUserStatus/communityUserStatus.providers';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Session]),
    TypeOrmModule.forFeature([Message]),
    TypeOrmModule.forFeature([CommunityUserStatus]),
  ],
  controllers: [UsersController],
  providers: [
    UsersService,
    ...usersProviders,
    ...appProviders,
    ...communityUserStatusProviders,
  ],
  exports: [UsersService],
})
export class UsersModule {}
