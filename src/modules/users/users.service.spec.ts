import { Test, TestingModule } from '@nestjs/testing';
import { createConnection } from 'typeorm';
import { UsersService } from './users.service';
import { User } from './user.entity';
import ethSigUtil from 'eth-sig-util';
import ethUtil from 'ethereumjs-util';
import { getRepositoryToken } from '@nestjs/typeorm';
import {
  newUser,
  newCommunity,
  newSession,
  newUser2,
} from '../../common/helpers/test/fixtures';
import { Community } from '../communities/community.entity';
import { Session } from '../session/session.entity';
import { Message } from '../messages/message.entity';
import { Messages } from '../messages/messages.enum';
import { SessionsService } from '../session/sessions.service';
import { Statuses } from './statuses.enum';
import { MessagesService } from '../messages/messages.service';
import { CommunitiesService } from '../communities/communities.service';
import { S3uploaderService } from '../s3uploader/s3uploader.service';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';
import { MailerService } from '../mailer/mailer.service';

describe('UsersService', () => {
  let usersModule: TestingModule;
  let usersService: UsersService;
  let sessionsService: SessionsService;
  let messagesService: MessagesService;
  let communitiesService: CommunitiesService;
  let s3uploaderService: S3uploaderService;
  let mailerService: MailerService;
  let usersRepository;
  let communitiesRepository;
  let sessionsRepository;
  let messagesRepository;
  let savedTestUser: User;

  beforeAll(async () => {
    const testConnection = await createConnection({
      type: 'sqljs',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      logging: false,
      dropSchema: true,
      synchronize: true,
    });
    const communityUserStatusRepository = testConnection.getRepository(
      CommunityUserStatus,
    );
    usersRepository = testConnection.getRepository(User);
    communitiesRepository = testConnection.getRepository(Community);
    sessionsRepository = testConnection.getRepository(Session);
    messagesRepository = testConnection.getRepository(Message);

    usersModule = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(CommunityUserStatus),
          useValue: communityUserStatusRepository,
        },
        MailerService,
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        SessionsService,
        {
          provide: getRepositoryToken(Session),
          useValue: sessionsRepository,
        },
        MessagesService,
        {
          provide: getRepositoryToken(Message),
          useValue: messagesRepository,
        },
        CommunitiesService,
        {
          provide: getRepositoryToken(Community),
          useValue: communitiesRepository,
        },
        S3uploaderService,
      ],
    }).compile();

    usersService = usersModule.get<UsersService>(UsersService);
    sessionsService = usersModule.get<SessionsService>(SessionsService);
    messagesService = usersModule.get<MessagesService>(MessagesService);
    communitiesService = usersModule.get<CommunitiesService>(
      CommunitiesService,
    );
    s3uploaderService = usersModule.get<S3uploaderService>(S3uploaderService);
  });

  beforeAll(async () => {
    await messagesService.createAndSave({ text: Messages.Staked });
    await messagesService.createAndSave({ text: Messages.Unstaked });
  });

  beforeEach(async () => {
    savedTestUser = await usersService.createAndSave(newUser);
  });

  describe('create', () => {
    it('should create and save user', async () => {
      expect(savedTestUser.address).toBe(newUser.address);
    });

    it('should update and save member ', async () => {
      await usersService.createAndSave(newUser2);
      const user = await usersService.findOneByAddress(newUser2.address);
      await usersService.save(user);
      const result = await usersService.updateAndSaveMember(user.id, {
        id: user.id,
        country: 'US',
        alias: 'Dylon Dylon',
      });
      expect(result.country).toBe('US');
      expect(result.alias).toBe('Dylon Dylon');
    });

    it('should not update a member address or id', async () => {
      await usersService.createAndSave(newUser2);
      const user = await usersService.findOneByAddress(newUser2.address);
      await usersService.save(user);
      const result = await usersService.updateAndSaveMember(user.id, {
        id: 99999,
        address: '0x0',
        alias: 'Dylon Dylon',
      });
      expect(result.id).toBe(user.id);
      expect(result.address).toBe(user.address);
    });
  });

  describe('getMessageToSign', () => {
    it('should return the correct message', async () => {
      const message = await usersService.getMessageToSign(newUser.address);
      expect(message).toBe(
        'By signing this transaction you are agreeing to the terms of service ' +
          'at https://app.nativeproject.one/terms as the user of the Ethereum address: ' +
          newUser.address,
      );
    });

    it('should create a user if none found', async () => {
      const nonExistentAddress = '0xTestAddress';
      await usersService.getMessageToSign(nonExistentAddress);
      const user = await usersService.findOneByAddress(nonExistentAddress);
      expect(user.address).toBe(nonExistentAddress);
    });
  });

  describe('authenticateUser', () => {
    const newUserPrivateKey =
      '0xfa79a3e3e7250878b28e0449a0ca4ef215739fdc246a795d297adec587d40b18';

    it('should authenticate user if the signature matches', async () => {
      const signature = ethSigUtil.personalSign(
        ethUtil.toBuffer(newUserPrivateKey),
        {
          data:
            'By signing this transaction you are agreeing to the terms of service ' +
            'at https://app.nativeproject.one/terms as the user of the Ethereum address: ' +
            newUser.address,
        },
      );
      const response = await usersService.authenticateUser(
        signature,
        newUser.address,
      );
      expect(response.address).toBe(newUser.address);
    });

    it('should not authenticate user if the signature does not match', async () => {
      const signature = ethSigUtil.personalSign(
        ethUtil.toBuffer(newUserPrivateKey),
        { data: 'Invalid message to sign' },
      );
      const response = await usersService.authenticateUser(
        signature,
        newUser.address,
      );
      expect(response).toBeFalsy();
    });
  });

  describe('removeSigningAndSave', () => {
    it('should remove the message the user is signing', async () => {
      savedTestUser.signing = 'Test Message';
      await usersService.removeSigningAndSave(savedTestUser);
      const returnedUser = await usersService.findOneByAddress(newUser.address);
      expect(returnedUser.signing).toBeFalsy();
    });
  });

  describe('setStatusToPending', () => {
    it('should set a users status to pending', async () => {
      await usersService.setStatusToPending(newUser.address);
      const returnedUser = await usersService.findOneByAddress(newUser.address);
      expect(returnedUser.status).toBe(Statuses.Pending);
    });
  });

  describe('staking', () => {
    it('should stake a user as a member of a community', async () => {
      const thisCommunity = await communitiesRepository.save(newCommunity);
      const thisUser = await usersRepository.save(newUser);
      const thisSession = await sessionsService.createAndSave(newSession);
      await usersService.stake(thisUser.address, thisCommunity);
      const foundUser = await usersService.findOneByAddress(thisUser.address);
      expect((foundUser.memberOf[0].id = thisCommunity.id));
      expect((foundUser.memberOf[0].name = thisCommunity.name));
    });

    it('should unstake a user as a member of a community', async () => {
      const thisCommunity = await communitiesRepository.save(newCommunity);
      const thisUser = await usersRepository.save(newUser);
      const thisSession = await sessionsService.createAndSave(newSession);
      await usersService.stake(thisUser.address, thisCommunity);
      const foundUser = await usersService.findOneByAddress(thisUser.address);
      expect((foundUser.memberOf[0].id = thisCommunity.id));
      expect((foundUser.memberOf[0].name = thisCommunity.name));
      foundUser.status = Statuses.Pending;
      await usersService.unstake(foundUser.address, thisCommunity);
      const foundUser2 = await usersService.findOneByAddress(thisUser.address);
      expect(foundUser2.messages.length > 0);
      expect(foundUser2.messages[0].text).toBe(Messages.Unstaked);
      expect(foundUser2.memberOf.length < 1);
      expect(foundUser2.status).toBe(null);
    });

    it('should stake and unstake a user as a curator of a community', async () => {
      const thisCommunity = await communitiesRepository.save(newCommunity);
      const thisUser = await usersRepository.save(newUser);
      const thisSession = await sessionsService.createAndSave(newSession);
      await usersService.addCurator(thisUser.address, thisCommunity);

      const foundCurator = await usersService.findOneByAddress(
        thisUser.address,
      );
      expect((foundCurator.curatorOf[0].id = thisCommunity.id));

      await usersService.stake(thisUser.address, thisCommunity);
      const foundUser = await usersService.findOneByAddress(thisUser.address);
      expect((foundUser.memberOf[0].id = thisCommunity.id));
      expect((foundUser.memberOf[0].name = thisCommunity.name));
      expect((foundUser.curatorOf[0].id = thisCommunity.id));
      foundUser.status = Statuses.Pending;
      await usersService.unstake(foundUser.address, thisCommunity);
      const foundUser2 = await usersService.findOneByAddress(thisUser.address);
      expect(foundUser2.messages.length > 0);
      expect(foundUser2.messages[0].text).toBe(Messages.Unstaked);
      expect(foundUser2.memberOf.length < 1);
      expect(foundUser2.status).toBe(null);
      expect(foundUser2.curatorOf.length < 1);
    });

    it('should reset a session after staking', async () => {
      const thisCommunity = await communitiesRepository.save(newCommunity);
      const thisUser = await usersRepository.save(newUser);
      const thisSession = await sessionsService.createAndSave(newSession);
      await usersService.stake(thisUser.address, thisCommunity);
      const foundUser = await usersService.findOneByAddress(thisUser.address);
      const foundSession = await sessionsService.findOneByUser(foundUser);
      const foundSessionJson = JSON.parse(foundSession.json);
      expect(foundSessionJson.user.memberOf.length > 0);
    });

    it('should return an array of messages for a user', async () => {
      const thisCommunity = await communitiesRepository.save(newCommunity);
      const thisUser = await usersRepository.save(newUser);
      const thisSession = await sessionsService.createAndSave(newSession);
      await usersService.stake(thisUser.address, thisCommunity);
      const foundUser = await usersService.findOneByAddress(thisUser.address);
      const foundSession = await sessionsService.findOneByUser(foundUser);
      const foundSessionJson = JSON.parse(foundSession.json);
      expect(foundUser.messages.length > 0);
      expect(foundUser.messages[0].text).toBe(Messages.Staked);
    });

    it('should remove a message', async () => {
      const thisCommunity = await communitiesRepository.save(newCommunity);
      const thisUser = await usersRepository.save(newUser);
      const thisSession = await sessionsService.createAndSave(newSession);
      await usersService.stake(thisUser.address, thisCommunity);
      const foundUser = await usersService.findOneByAddress(thisUser.address);
      const foundSession = await sessionsService.findOneByUser(foundUser);
      const foundSessionJson = JSON.parse(foundSession.json);
      expect(foundUser.messages.length > 0);
      expect(foundUser.messages[0].text).toBe(Messages.Staked);
      await usersService.removeMessage(foundUser.messages[0].id, foundUser.id);
      const foundUser2 = await usersService.findOneByAddress(thisUser.address);
      expect(foundUser2.messages.length < 1);
    });
  });
});
