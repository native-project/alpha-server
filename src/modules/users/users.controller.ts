import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Patch,
  Post,
  Request,
  Response,
  Session,
  UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { User } from './user.entity';
import { authenticatedSessionUser } from './permission.helpers';

@UseGuards(RolesGuard)
@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  @Get('')
  async getUser(@Request() request, @Session() session) {
    const sessionUser = session.user;
    if (!sessionUser || !sessionUser.address) {
      return { user: '' };
    }
    const user = await this.usersService.findOneByAddress(sessionUser.address);
    return user ? { user } : '';
  }

  @Post('dismiss')
  async removeMessage(@Body() body, @Session() session) {
    const sessionUser = authenticatedSessionUser(session);
    if (!body.message) {
      throw new HttpException(
        'You must supply the message to remove.',
        HttpStatus.BAD_REQUEST,
      );
    }
    return await this.usersService.removeMessage(body.message, sessionUser.id);
  }

  @Post('signing')
  async getMessageToSign(@Body() body) {
    if (!body.address) {
      throw new HttpException(
        'You must supply an address.',
        HttpStatus.BAD_REQUEST,
      );
    }
    return await this.usersService.getMessageToSign(body.address);
  }

  @Post('authorize')
  async authenticate(@Body() body, @Session() session) {
    if (!body.address || !body.signature) {
      throw new HttpException(
        'You must supply an address and signature.',
        HttpStatus.BAD_REQUEST,
      );
    }
    const authenticatedUser = await this.usersService.authenticateUser(
      body.signature,
      body.address,
    );
    if (authenticatedUser) {
      session.user = authenticatedUser;
    }
    return authenticatedUser !== null ? authenticatedUser : '';
  }

  @Post('end-session')
  async endSession(@Session() session, @Response() response) {
    try {
      session.destroy();
      response
        .clearCookie(process.env.APP_NAME, { path: '/' })
        .status(200)
        .send('Session cleared.');
    } catch (err) {
      throw new HttpException(
        'There was a problem ending the session',
        HttpStatus.BAD_REQUEST,
      );
    }
    return response;
  }

  @Patch(':id')
  @Roles('member', 'curator', 'admin')
  async updateMember(@Body() userToUpdate: Partial<User>, @Session() session) {
    const sessionUser = authenticatedSessionUser(session);
    const user = await this.usersService.findOneByAddress(sessionUser.address);
    if (user.id !== userToUpdate.id) {
      throw new HttpException(
        'You are not able to edit that user.',
        HttpStatus.UNAUTHORIZED,
      );
    }
    return await this.usersService.updateAndSaveMember(
      userToUpdate.id,
      userToUpdate,
    );
  }

  @Get(':address')
  async findOne(@Param() params, @Session() session) {
    return await this.usersService.findOneByAddress(params.address);
  }
}
