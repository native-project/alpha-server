import { HttpException, HttpStatus } from '@nestjs/common';
import { Statuses } from './statuses.enum';
import { User } from './user.entity';

export function authenticatedSessionUser(session) {
  if (!session.user) {
    throw new HttpException(
      'You must be authenticated to complete this action.',
      HttpStatus.UNAUTHORIZED,
    );
  }
  return session.user;
}

export async function permissionedCurator(
  session,
  communityId,
  usersService,
): Promise<User> {
  authenticatedSessionUser(session);
  const user = await usersService.findOneByAddress(session.user.address);

  checkCuratorOf(user, communityId);

  return user;
}

export async function permissionedMember(
  session,
  communityId,
  usersService,
): Promise<User> {
  authenticatedSessionUser(session);
  const user = await usersService.findOneByAddress(session.user.address);

  checkMemberOf(user, communityId);
  checkBlacklisted(user, communityId);
  checkPending(user);

  return user;
}

export function checkMemberOf(user, communityId) {
  const memberOf = user.memberOf.find((memberCommunity) => {
    return memberCommunity.id === communityId;
  });
  if (!memberOf) {
    throw new HttpException(
      'You must be a member of this community to perform that action.',
      HttpStatus.UNAUTHORIZED,
    );
  }
}

export function checkCuratorOf(user, communityId) {
  const curatorOf = user.curatorOf.find((curatorCommunity) => {
    return curatorCommunity.id === +communityId;
  });
  if (!curatorOf) {
    throw new HttpException(
      'You must be a curator of this community to perform that action.',
      HttpStatus.UNAUTHORIZED,
    );
  }
}

export function checkBlacklisted(user, communityId) {
  const blacklistedFrom = user.blacklistedFrom.find((community) => {
    return community.id === communityId;
  });
  if (blacklistedFrom) {
    throw new HttpException(
      'You are not authorized to perform this action.',
      HttpStatus.UNAUTHORIZED,
    );
  }
}

export function checkPending(user) {
  const pending = user.status === Statuses.Pending;
  if (pending) {
    throw new HttpException(
      'You cannot perform this action while a transaction is pending.',
      HttpStatus.UNAUTHORIZED,
    );
  }
}
