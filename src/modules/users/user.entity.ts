import {
  Entity,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Vote } from '../votes/vote.entity';
import { Roles } from '../roles/roles.enum';
import { Task } from '../tasks/task.entity';
import { Community } from '../communities/community.entity';
import { Statuses } from './statuses.enum';
import { Message } from '../messages/message.entity';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn() id: number;

  @Column() address: string;

  @Column({ nullable: true })
  alias: string;

  @Column({ nullable: true })
  country: string;

  @Column({ nullable: true })
  state: string;

  @Column({ nullable: true })
  city: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true })
  telegram: string;

  @Column({ nullable: true })
  preferredContact: string;

  @Column({ nullable: true })
  signing: string;

  @Column({ type: 'varchar', nullable: true })
  role: Roles;

  @Column({ type: 'varchar', nullable: true })
  status: Statuses;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;

  @OneToMany((type) => Vote, (vote) => vote.poll)
  votes: Vote[];

  @OneToMany((type) => Task, (task) => task.claimedBy)
  tasks: Task[];

  @ManyToMany((type) => Message, (message) => message.user)
  @JoinTable({ name: 'user_messages' })
  messages: Message[];

  @ManyToMany((type) => Community, (community) => community.members)
  @JoinTable({ name: 'community_members' })
  memberOf: Community[];

  @ManyToMany((type) => Community, (community) => community.curators)
  @JoinTable({ name: 'community_curators' })
  curatorOf: Community[];

  @ManyToMany((type) => Community, (community) => community.blacklisted)
  @JoinTable({ name: 'community_blacklists' })
  blacklistedFrom: Community[];

  userStatus: string;

  communityStatusOf: CommunityUserStatus[];
}
