import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserDto } from './user.dto';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';
import { Message } from '../messages/message.entity';
import { Messages } from '../messages/messages.enum';
import { Community } from '../communities/community.entity';
import { recoverPersonalSignature } from 'eth-sig-util';
import { validate } from 'class-validator';
import { Session } from '../session/session.entity';
import { Roles } from '../roles/roles.enum';
import { Statuses } from './statuses.enum';
import { SessionsService } from '../session/sessions.service';
import { MessagesService } from '../messages/messages.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    @InjectRepository(Message)
    private readonly messagesRepository: Repository<Message>,
    @InjectRepository(CommunityUserStatus)
    private readonly communityUserStatusRepository: Repository<
      CommunityUserStatus
    >,
    @InjectRepository(Session)
    private readonly sessionsRepository: Repository<Session>,
  ) {}
  messagesService = new MessagesService(this.messagesRepository);
  sessionsService = new SessionsService(this.sessionsRepository);

  async setStatusToPending(address) {
    const user: User = await this.findOneByAddress(address);
    user.status = Statuses.Pending;
    return await this.usersRepository.save(user);
  }

  async getMessageToSign(address) {
    let user = await this.findOneByAddress(address);
    if (!user) {
      const userDto = {
        address,
      };
      user = await this.createAndSave(userDto);
    }
    user.signing =
      'By signing this transaction you are agreeing to the terms of service at' +
      ' https://app.nativeproject.one/terms as the user of the Ethereum address: ' +
      user.address;
    await this.usersRepository.save(user);
    return user.signing;
  }

  async authenticateUser(signedMessage, address): Promise<User | null> {
    const user: User = await this.findOneByAddress(address);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: `No user found.`,
        },
        404,
      );
    }
    const message = user.signing;
    await this.removeSigningAndSave(user);
    const recovered = recoverPersonalSignature({
      data: message,
      sig: signedMessage,
    });
    return recovered === user.address.toLowerCase() ? user : null;
  }

  async updateAndSaveMember(
    userId: number,
    userDto: Partial<User>,
  ): Promise<User> {
    delete userDto.address;
    delete userDto.id;
    const errors = await validate(userDto);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const result = await this.usersRepository.update(userId, userDto);
    return await this.findOne(userId);
  }

  async createAndSave(userDto: UserDto): Promise<User> {
    const errors = await validate(userDto);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const user: User = this.create(userDto);
    return await this.usersRepository.save(user);
  }

  create(userDto: UserDto): User {
    return this.usersRepository.create(userDto);
  }

  async save(user: User): Promise<User> {
    return await this.usersRepository.save(user);
  }

  async removeMessage(messageId, userId): Promise<User> {
    const message = await this.messagesRepository.findOne(messageId);
    if (!message) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: `No message found.`,
        },
        404,
      );
    }
    const user = await this.findOne(userId);
    if (!user) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: `No user found.`,
        },
        404,
      );
    }
    user.messages = user.messages.filter((m) => {
      return m.id !== messageId;
    });
    const savedUser = await this.usersRepository.save(user);
    const savedSession = await this.sessionsService.resetSession(savedUser);
    if (!savedSession) {
      throw new Error('Unable to reauthenticate');
    }
    if (!savedUser) {
      throw new Error('User unable to be saved.');
    }
    return savedUser;
  }

  async removeSigningAndSave(user: User): Promise<void> {
    const errors = await validate(user);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    user.signing = null;
    await this.usersRepository.save(user);
  }

  async findOneByAddress(address): Promise<User | null> {
    const user = await this.usersRepository.findOne({
      where: { address },
      relations: ['memberOf', 'curatorOf', 'messages', 'blacklistedFrom'],
    });
    const populatedUser = await this.populateCommunityStatus(user);
    return populatedUser ? populatedUser : null;
  }

  async findOne(id): Promise<User | null> {
    const user = await this.usersRepository.findOne(id, {
      relations: ['memberOf', 'curatorOf', 'messages', 'blacklistedFrom'],
    });
    const populatedUser = await this.populateCommunityStatus(user);
    return populatedUser ? populatedUser : null;
  }

  async preload(partialUser: Partial<User>): Promise<User | null> {
    const preloadedUser = await this.usersRepository.preload(partialUser);
    delete preloadedUser.updatedAt;
    return preloadedUser;
  }

  async unstake(userAddress: string, community: Community): Promise<User> {
    const user = await this.findOneByAddress(userAddress);
    if (!user) {
      throw new Error('No user with that address found.');
    }
    user.status = null;
    const communitiesArray = user.memberOf;
    communitiesArray.splice(communitiesArray.indexOf(community), 1);
    user.memberOf = communitiesArray;
    const message = await this.messagesService.findOneByText(Messages.Unstaked);
    user.messages = [];
    user.messages.push(message);
    const savedUser = await this.usersRepository.save(user);
    const savedSession = await this.sessionsService.resetSession(savedUser);
    if (!savedSession) {
      throw new Error('Unable to reauthenticate');
    }
    if (!savedUser) {
      throw new Error('User unable to be saved.');
    }
    return user;
  }

  async stake(userAddress: string, community: Community): Promise<User> {
    const user = await this.findOneByAddress(userAddress);
    if (!user) {
      throw new Error('No user with that address found.');
    }
    const communitiesArray = user.memberOf;
    communitiesArray.push(community);
    user.memberOf = communitiesArray;

    if (user.role !== Roles.Curator && user.role !== Roles.Admin) {
      user.role = Roles.Member;
    }

    const message = await this.messagesService.findOneByText(Messages.Staked);
    user.messages = user.messages.filter((m) => {
      return m.text !== Messages.PreStake;
    });
    user.messages.push(message);
    const savedUser = await this.usersRepository.save(user);
    const savedSession = await this.sessionsService.resetSession(savedUser);
    if (!savedSession) {
      throw new Error('Unable to reauthenticate');
    }
    if (!savedUser) {
      throw new Error('User unable to be saved.');
    }
    return user;
  }

  async preStake(userAddress: string): Promise<User> {
    const user = await this.findOneByAddress(userAddress);
    if (!user) {
      throw new Error('No user with that address found.');
    }

    const message = await this.messagesService.findOneByText(Messages.PreStake);
    user.messages.push(message);
    const savedUser = await this.usersRepository.save(user);
    const savedSession = await this.sessionsService.resetSession(savedUser);
    if (!savedSession) {
      throw new Error('Unable to reauthenticate');
    }

    if (!savedUser) {
      throw new Error('User unable to be saved.');
    }
    return user;
  }

  async addCurator(
    curatorAddress: string,
    community: Community,
  ): Promise<User> {
    const user = await this.findOneByAddress(curatorAddress);
    if (!user) {
      throw new Error('No user with that address found.');
    }
    const communitiesArray = user.curatorOf;
    communitiesArray.push(community);
    user.curatorOf = communitiesArray;
    user.role = Roles.Curator;
    const savedUser = await this.usersRepository.save(user);
    const savedSession = await this.sessionsService.resetSession(savedUser);
    if (!savedSession) {
      throw new Error('Unable to reauthenticate');
    }
    if (!savedUser) {
      throw new Error('User unable to be saved.');
    }
    return user;
  }

  async populateCommunityStatus(user): Promise<User> {
    if (user) {
      user.communityStatusOf = await this.communityUserStatusRepository.find({
        where: { userId: user.id },
      });
    }
    return user;
  }
}
