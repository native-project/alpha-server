import { Test, TestingModule } from '@nestjs/testing';
import { MailerService } from './mailer.service';

describe('Mailer Service', () => {
  let mailerModule: TestingModule;
  let mailerService: MailerService;
  beforeAll(async () => {
    mailerModule = await Test.createTestingModule({
      providers: [MailerService],
    }).compile();

    mailerService = mailerModule.get<MailerService>(MailerService);
  });

  it('should be defined', () => {
    expect(mailerService).toBeDefined();
  });

  /* NOTE: These tests are NOT checking for the sending of an email.
   The most imporantaspect of the module is in the formatting of the request variable and as such, this will be tested */

  it('should get the options for an email', async () => {
    const sendToEmail = 'test@ibn.com';
    const subject = 'Test subject';
    const content = {
      type: 'text/plain',
      value: 'Test the body portion on an email',
    };
    let options = mailerService.formatEmailOptions(
      sendToEmail,
      subject,
      content,
    );
    expect(options.headers.authorization).toBeDefined;
    expect(options.body.from.email).toBeDefined;
    expect(options.body.reply_to.email).toBeDefined;

    // emptying this out as it relates only to the .env and not tests.
    options.headers.authorization = '';
    options.body.from.email = '';
    options.body.reply_to.email = '';

    const formattedOptions = {
      body: {
        content: [
          {
            type: 'text/plain',
            value: 'Test the body portion on an email',
          },
        ],
        from: {
          email: '',
        },
        mail_settings: {
          sandbox_mode: {
            enable: !process.env.MAILER_API_KEY,
          },
        },
        personalizations: [
          {
            subject: 'Test subject',
            to: [
              {
                email: 'test@ibn.com',
              },
            ],
          },
        ],
        reply_to: {
          email: '',
        },
      },
      headers: {
        authorization: '',
        'content-type': 'application/json',
      },
      json: true,
      method: 'POST',
      url: 'https://api.sendgrid.com/v3/mail/send',
    };

    expect(options).toEqual(formattedOptions);
  });
});
