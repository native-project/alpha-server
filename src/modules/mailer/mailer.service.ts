import { Injectable } from '@nestjs/common';
import request from 'request-promise';
import dotenv from 'dotenv';
dotenv.config();

const apikey = process.env.MAILER_API_KEY ? process.env.MAILER_API_KEY : '';
const mailerEmail = process.env.MAILER_EMAIL ? process.env.MAILER_EMAIL : '';

@Injectable()
export class MailerService {
  constructor() {}

  formatEmailOptions(sendToEmail, subject, content) {
    return {
      method: 'POST',
      url: 'https://api.sendgrid.com/v3/mail/send',
      headers: {
        'content-type': 'application/json',
        authorization: 'Bearer ' + apikey,
      },
      body: {
        personalizations: [
          {
            to: [{ email: sendToEmail }],
            subject,
          },
        ],
        from: { email: mailerEmail },
        reply_to: { email: mailerEmail },
        content: [content],
        mail_settings: {
          sandbox_mode: {
            enable: !process.env.MAILER_API_KEY,
          },
        },
      },
      json: true,
    };
  }

  async sendRequest(options) {
    if (process.env.MAILER_API_KEY) {
      return await request(options);
    }
  }

  async sendRequestAccessEmail(curatorEmail, user, message) {
    const subject = 'Access Requested: ' + user.address;
    const content = {
      type: 'text/plain',
      value:
        'User with address:' +
        user.address +
        ' has requested to join your community. User message: ' +
        message,
    };
    const options = this.formatEmailOptions(curatorEmail, subject, content);

    try {
      return await this.sendRequest(options);
    } catch (error) {
      throw new Error(error);
    }
  }

  async sendTaskClaimedEmail(task) {
    const subject = 'Task claimed: ' + task.id;
    const content = {
      type: 'text/plain',
      value: 'Task ' + task.id + ' was claimed by ' + task.claimedBy.address,
    };
    const options = this.formatEmailOptions(task.userEmail, subject, content);

    try {
      return await this.sendRequest(options);
    } catch (error) {
      throw new Error(error);
    }
  }

  async sendTaskEscrowedEmail(sendToEmail, task) {
    const subject = 'Task Escrowed: ' + task.id;
    const content = {
      type: 'text/plain',
      value: 'Task ' + task.id + ' was excrowed',
    };
    const options = this.formatEmailOptions(sendToEmail, subject, content);

    try {
      return await this.sendRequest(options);
    } catch (error) {
      throw new Error(error);
    }
  }

  async sendTaskCompleteEmail(sendToEmail, task) {
    const subject = 'Task Completed: ' + task.id;
    const content = {
      type: 'text/plain',
      value: 'Task ' + task.id + ' was completed',
    };
    const options = this.formatEmailOptions(sendToEmail, subject, content);

    try {
      return await this.sendRequest(options);
    } catch (error) {
      throw new Error(error);
    }
  }

  async sendTaskSubmissionDenied(sendToEmail, task) {
    const subject = 'Task Submission Denied: ' + task.title;
    const content = {
      type: 'text/plain',
      value: 'Submission for task: "' + task.title + '" was denied',
    };
    const options = this.formatEmailOptions(sendToEmail, subject, content);

    try {
      return await this.sendRequest(options);
    } catch (error) {
      throw new Error(error);
    }
  }

  async sendTaskCanceledEmail(sendToEmail, task) {
    const subject = 'Task Canceled: ' + task.id;
    const content = {
      type: 'text/plain',
      value: 'Task ' + task.id + ' was canceled',
    };
    const options = this.formatEmailOptions(sendToEmail, subject, content);

    try {
      return await this.sendRequest(options);
    } catch (error) {
      throw new Error(error);
    }
  }
}
