import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { communityUserStatusProviders } from './communityUserStatus.providers';
import { CommunityUserStatus } from './communityUserStatus.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CommunityUserStatus])],
  providers: [...communityUserStatusProviders],
})
export class CommunityUserStatusModule {}
