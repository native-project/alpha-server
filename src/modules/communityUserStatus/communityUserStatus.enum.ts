export enum communityUserStatus {
  Pending = 'pending',
  Denied = 'denied',
  Approved = 'approved',
  Blacklist = 'blacklisted',
  Member = 'member',
}
