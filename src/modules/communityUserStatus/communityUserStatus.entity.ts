import { Entity, Column, PrimaryColumn } from 'typeorm';

@Entity()
export class CommunityUserStatus {
  @PrimaryColumn() userId: number;

  @Column({ nullable: true })
  communityId: number;

  @Column({ nullable: true })
  userStatus: string;
}
