import { Connection } from 'typeorm';
import { CommunityUserStatus } from './communityUserStatus.entity';

export const communityUserStatusProviders = [
  {
    provide: 'CommunityUserStatusRepository',
    useFactory: (connection: Connection) =>
      connection.getRepository(CommunityUserStatus),
    inject: [Connection],
  },
];
