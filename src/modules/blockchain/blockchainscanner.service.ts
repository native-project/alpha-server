import { Injectable, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';
import { LastBlock } from './lastBlock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import Web3 from 'web3';
import { lastBlockTypes } from './lastBlockTypes.enum';
import { EventsService } from '../events/events.service';
import { BlockchainEventService } from './blockchainEvent.service';

@Injectable()
export class BlockchainScannerService {
  constructor(
    @Inject('Web3') private readonly web3: Web3,
    @InjectRepository(LastBlock)
    private lastBlocksRepository: Repository<LastBlock>,
    private eventsService: EventsService,
    private blockchainEventService: BlockchainEventService,
  ) {
    this.web3 = web3;
    this.blockchainEventService = blockchainEventService;
    this.lastBlocksRepository = lastBlocksRepository;
  }

  async getDatabaseBlockNumber(type: lastBlockTypes): Promise<number> {
    const lastBlocks = await this.lastBlocksRepository.find({
      where: { type },
    });
    if (lastBlocks.length === 0) {
      throw new Error('You must supply a starting block.');
    } else {
      return lastBlocks[0].lastBlock;
    }
  }

  async updateLastBlockScanned(lastBlockScanned: number, type: lastBlockTypes) {
    const lastBlocks = await this.lastBlocksRepository.find({
      where: { type },
    });
    if (lastBlocks.length <= 0) {
      throw new Error('You must supply a starting block.');
    } else {
      const lastBlock = lastBlocks[0];
      lastBlock.lastBlock = lastBlockScanned;
      await this.lastBlocksRepository.save(lastBlock);
    }
  }

  // helper function because the generic log does not correctly pass back the data like the rest of the events.
  // Other events store it under the returnResults object while genericLog returns it under a hex encoded blob
  decodeGenericLog(event: any): any {
    // return early, we already have a good event and do not need to decode
    if (event.event !== undefined) {
      return event;
    }
    const decodedLog = this.web3.eth.abi.decodeLog(
      [
        {
          type: 'address',
          name: 'msgSender',
        },
        {
          type: 'string',
          name: 'messageType',
        },
        {
          type: 'string',
          name: 'message',
        },
      ],
      event.raw.data,
      event.raw.topics,
    );
    // technically this may not be a generic log but it is a reasonable assumption that it is.
    // if anyone comes across a solution to check if it is _not_ a generic log, please feel free to update.
    event.event = 'GenericLog';
    event.returnValues = decodedLog;
    return event;
  }

  async scan(communityContractsService, scanType: lastBlockTypes) {
    if (
      scanType === lastBlockTypes.Confirmation &&
      !process.env.CONFIRMATION_TOLERANCE
    ) {
      throw new Error('You must set a confirmation tolerance');
    }
    // We get both the last scanned block(fromDB) and the last block(from chain)
    const lastScannedBlock = await this.getDatabaseBlockNumber(scanType);
    const lastBlock = await communityContractsService.getNodeBlockNumber();
    // if confirm scan, set to (n - tolerance level)
    const safeBlock =
      scanType === lastBlockTypes.Confirmation
        ? lastBlock - parseInt(process.env.CONFIRMATION_TOLERANCE, 10)
        : lastBlock;

    // Check if we are scanned up to the lastSafeBlock yet or not. If we have not scanned that far, we scan that block.
    if (lastScannedBlock < safeBlock) {
      const blockToScan = lastScannedBlock + 1;
      const blockWithTransactions = await communityContractsService.getBlockWithTransactions(
        blockToScan,
      );

      if (blockWithTransactions) {
        // on the specific block we are looking at, grab all of the events
        const pastEvents = await communityContractsService.getPastEvents(
          'allEvents',
          blockToScan,
        );
        await this.processEvents(pastEvents, scanType);
      }
      await this.updateLastBlockScanned(blockToScan, scanType);
    }
  }
  async processEvents(pastEvents: any, scanType: string) {
    for (let event of pastEvents) {
      event = this.decodeGenericLog(event);
      if (event.event) {
        switch (scanType) {
          case lastBlockTypes.Confirmation:
            this.eventsService.handleEventConfirmation(event);
            this.blockchainEventService.saveEvent(event);
            break;
          case lastBlockTypes.Head:
            this.eventsService.handleEventHead(event);
            break;
          default:
        }
      }
    }
  }
}
