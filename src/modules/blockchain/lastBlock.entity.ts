import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Unique,
} from 'typeorm';
import { lastBlockTypes } from './lastBlockTypes.enum';

@Entity()
@Unique(['type'])
export class LastBlock {
  @PrimaryGeneratedColumn() id: number;

  @Column() lastBlock: number;

  @Column({ type: 'varchar' })
  type: lastBlockTypes;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;
}
