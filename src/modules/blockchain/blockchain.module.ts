import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlockchainScannerService } from './blockchainscanner.service';
import { CommunityContractsService } from './communitycontracts.service';
import { lastBlockProviders } from './lastBlock.providers';
import { blockchainEventProviders } from './blockchainEvent.providers';
import { web3Providers } from './web3.providers';
import { LastBlock } from './lastBlock.entity';
import { BlockchainEventService } from './blockchainEvent.service';
import { EventsModule } from '../events/events.module';

@Module({
  imports: [EventsModule, TypeOrmModule.forFeature([LastBlock])],
  controllers: [],
  providers: [
    BlockchainScannerService,
    BlockchainEventService,
    CommunityContractsService,
    ...web3Providers,
    ...lastBlockProviders,
    ...blockchainEventProviders,
  ],
  exports: [BlockchainScannerService],
})
export class BlockchainModule {}
