import { Connection } from 'typeorm';
import { LastBlock } from './lastBlock.entity';

export const lastBlockProviders = [
  {
    provide: 'LastBlockRepository',
    useFactory: (connection: Connection) => connection.getRepository(LastBlock),
    inject: [Connection],
  },
];
