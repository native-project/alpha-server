import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Unique,
} from 'typeorm';

@Entity()
@Unique(['logID'])
export class BlockchainEvent {
  @PrimaryGeneratedColumn() id: number;

  @Column() transactionHash: string;

  @Column() logID: string;

  @Column() blockNumber: number;

  @Column() event: string;

  @Column() address: string;

  @Column() returnValues: string;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;
}
