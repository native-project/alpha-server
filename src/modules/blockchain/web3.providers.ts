import Web3 from 'web3';

function setWeb3() {
  const web3Provider = new Web3.providers.HttpProvider(
    process.env.WEB3_PROVIDER,
  );
  return new Web3(web3Provider);
}

export const web3Providers = [
  {
    provide: 'Web3',
    useFactory: () => setWeb3(),
  },
];
