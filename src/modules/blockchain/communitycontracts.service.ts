import { Injectable, Inject } from '@nestjs/common';

import Web3 from 'web3';
import communityJson from './abi/Community.json';
import loggerJson from './abi/Logger.json';
import fs from 'fs-extra';
import path from 'path';

@Injectable()
export class CommunityContractsService {
  constructor(@Inject('Web3') private readonly web3: Web3) {
    this.web3 = web3;
  }
  async getAccounts() {
    const accounts = await this.web3.eth.getAccounts();
    return accounts;
  }

  async syncAbis() {
    try {
      const contractRepositoryPath = process.env.CONTRACT_REPOSITORY_PATH;
      await fs.copy(
        path.resolve(contractRepositoryPath, 'build/contracts'),
        path.resolve(__dirname, './abi'),
      );
    } catch (err) {
      console.log(err); // tslint:disable-line
    }
    return true;
  }

  async isCommunityMember(
    communityContractAddress: string,
    userAddress: string,
  ) {
    const accounts = this.getAccounts();
    const communityAbi = communityJson.abi;
    const communityContract = await new this.web3.eth.Contract(
      communityAbi,
      communityContractAddress,
    );
    return await communityContract.methods
      .isMember(userAddress)
      .call({ from: accounts[0] });
  }
  async getPastEvents(eventType: string, blockNumber: number): Promise<any[]> {
    if (!process.env.LOGGER_CONTRACT_ADDRESS) {
      throw new Error('You must set the Logger smart contract address');
    }
    if (blockNumber < 1) {
      throw new Error('Block number cannot be less than 1');
    }
    let events = [];
    const loggerAbi = loggerJson.abi;
    const loggerContract = await new this.web3.eth.Contract(
      loggerAbi,
      process.env.LOGGER_CONTRACT_ADDRESS,
    );
    await loggerContract.getPastEvents(
      eventType,
      {
        fromBlock: blockNumber,
        toBlock: blockNumber,
      },
      (error, event) => {
        if (error) {
          console.log('Error:', error); // tslint:disable-line
        }
        events = event;
      },
    );
    return events;
  }

  async getNodeBlockNumber() {
    return await this.web3.eth.getBlockNumber();
  }

  async getBlockWithTransactions(blockNumber: number) {
    return await this.web3.eth.getBlock(blockNumber, true);
  }
}
