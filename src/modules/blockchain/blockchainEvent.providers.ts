import { Connection } from 'typeorm';
import { BlockchainEvent } from './blockchainEvent.entity';

export const blockchainEventProviders = [
  {
    provide: 'BlockchainEventRepository',
    useFactory: (connection: Connection) =>
      connection.getRepository(BlockchainEvent),
    inject: [Connection],
  },
];
