import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BlockchainEvent } from './blockchainEvent.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BlockchainEventService {
  constructor(
    @InjectRepository(BlockchainEvent)
    private eventRepository: Repository<BlockchainEvent>,
  ) {
    this.eventRepository = eventRepository;
  }
  async saveEvent(ethereumEvent: any): Promise<any> {
    const event = new BlockchainEvent();
    event.transactionHash = ethereumEvent.transactionHash;
    event.address = ethereumEvent.address;
    event.blockNumber = ethereumEvent.blockNumber;
    event.returnValues = ethereumEvent.returnValues; // json
    event.event = ethereumEvent.event;
    event.logID = ethereumEvent.id; // used as unique identifier
    await this.eventRepository.save(event);
  }
}
