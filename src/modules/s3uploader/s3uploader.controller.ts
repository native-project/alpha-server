import { Controller, Req, Res, Get, Param } from '@nestjs/common';
import { S3uploaderService } from './s3uploader.service';

@Controller('community-assets')
export class S3uploaderController {
  constructor(private readonly s3UploadService: S3uploaderService) {}
  @Get()
  async findSignedUrl(@Req() request, @Res() response) {
    try {
      await this.s3UploadService.s3Upload(request, response);
    } catch (error) {
      return response
        .status(500)
        .json(`Failed to upload image file: ${error.message}`);
    }
  }

  @Get('uploads/:imgId')
  async s3Files(@Param('imgId') imgId, @Req() request, @Res() response) {
    try {
      await this.s3UploadService.s3UploadRedirect(imgId, request, response);
    } catch (error) {
      return response
        .status(500)
        .json(`Failed to get image file: ${error.message}`);
    }
  }
}
