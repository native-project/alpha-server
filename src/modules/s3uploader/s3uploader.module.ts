import { Module } from '@nestjs/common';
import { S3uploaderController } from './s3uploader.controller';
import { S3uploaderService } from './s3uploader.service';

@Module({
  controllers: [S3uploaderController],
  providers: [S3uploaderService],
  exports: [S3uploaderService],
})
export class S3uploaderModule {}
