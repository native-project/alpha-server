import { Test, TestingModule } from '@nestjs/testing';
import { S3uploaderController } from './s3uploader.controller';
import httpMocks from 'node-mocks-http';
import { S3uploaderService } from './s3uploader.service';

describe('S3uploader Controller', () => {
  let s3uploadermodule: TestingModule;
  let s3uploaderController: S3uploaderController;
  let s3uploaderService: S3uploaderService;
  const request = httpMocks.createRequest({
    method: 'GET',
    url: '/s3uploader',
  });
  const response = httpMocks.createResponse({});

  beforeAll(async () => {
    s3uploadermodule = await Test.createTestingModule({
      controllers: [S3uploaderController],
      providers: [S3uploaderService],
    }).compile();

    s3uploaderController = s3uploadermodule.get<S3uploaderController>(
      S3uploaderController,
    );
    s3uploaderService = s3uploadermodule.get<S3uploaderService>(
      S3uploaderService,
    );
  });

  it('should be defined', () => {
    expect(s3uploaderController).toBeDefined();
    expect(s3uploaderService).toBeDefined();
  });

  it('should get a singed url for image upload', async () => {
    const result = await s3uploaderController.findSignedUrl(request, response);
    expect(response.statusCode).toBe(200);
  });
});
