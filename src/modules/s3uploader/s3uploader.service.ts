import { Injectable, Req, Res } from '@nestjs/common';
import * as AWS from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';

const AWS_S3_BUCKET_NAME = process.env.AWS_S3_BUCKET_NAME;
const s3 = new AWS.S3();
AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

@Injectable()
export class S3uploaderService {
  constructor() {}

  checkTrailingSlash(path) {
    if (path && path[path.length - 1] !== '/') {
      path += '/';
    }
    return path;
  }

  s3Upload(@Req() request, @Res() response) {
    const filename: string = `${request.query.path || ''}${uuidv4().substring(
      0,
      8,
    )}_${request.query.objectName}`;
    const mimeType: string = request.query.contentType;
    const fileKey: string = filename;

    const params = {
      Bucket: AWS_S3_BUCKET_NAME,
      Key: fileKey,
      Expires: 60,
      ContentType: mimeType,
      ACL: 'private',
    };
    s3.getSignedUrl('putObject', params, (err, data) => {
      if (err) {
        return response.send(500, 'Cannot create S3 signed URL');
      }
      return response.status(201).json({
        signedUrl: data,
        publicUrl: '/community-assets/uploads/' + filename,
        filename,
        fileKey,
      });
    });
  }

  s3UploadRedirect(fileId: string, @Req() request, @Res() response) {
    const params = {
      Bucket: AWS_S3_BUCKET_NAME,
      Key: fileId,
    };

    s3.getSignedUrl('getObject', params, (err, url) => {
      if (err) {
        return response.send(500, 'Cannot get file');
      }
      return response.redirect(url);
    });
  }

  s3GetSignedUrl(fileId: string): string {
    const params = {
      Bucket: AWS_S3_BUCKET_NAME,
      Key: fileId,
    };

    return s3.getSignedUrl('getObject', params);
  }
}
