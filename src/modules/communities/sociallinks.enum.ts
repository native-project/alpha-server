export enum SocialLinks {
  Facebook = 'facebook',
  Twitter = 'twitter',
  Instagram = 'instagram',
}
