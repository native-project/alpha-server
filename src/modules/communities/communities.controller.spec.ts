import { Test, TestingModule } from '@nestjs/testing';
import { CommunitiesController } from './communities.controller';
import { CommunitiesService } from './communities.service';
import { TasksService } from '../tasks/tasks.service';
import { UsersService } from '../users/users.service';
import { MailerService } from '../mailer/mailer.service';
import { SessionsService } from '../session/sessions.service';
import { PollsService } from '../polls/polls.service';
import { VotesService } from '../votes/votes.service';
import { ProjectsService } from '../projects/projects.service';
import { MessagesService } from '../messages/messages.service';
import { OptionsService } from '../polls/options/options.service';
import { createConnection } from 'typeorm';
import { Community } from './community.entity';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';
import { Task } from '../tasks/task.entity';
import { User } from '../users/user.entity';
import { Session } from '../session/session.entity';
import { Message } from '../messages/message.entity';
import { Poll } from '../polls/poll.entity';
import { Option } from '../polls/options/option.entity';
import { Vote } from '../votes/vote.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Project } from '../projects/project.entity';
import {
  newCommunity,
  newUser,
  getSession,
  existingCurator,
  newUser2,
} from '../../common/helpers/test/fixtures';
import httpMocks from 'node-mocks-http';
import { S3uploaderService } from '../s3uploader/s3uploader.service';
import { web3Providers } from '../blockchain/web3.providers';

const request = httpMocks.createRequest({
  method: 'PUT',
  url: '/communities/:id',
});
const session = getSession(request, existingCurator.user);

describe('CommunitiesController', () => {
  let communitiesModule: TestingModule;
  let communitiesController: CommunitiesController;
  let tasksService: TasksService;
  let mailerService: MailerService;
  let communitiesService: CommunitiesService;
  let usersService: UsersService;
  let s3uploaderService: S3uploaderService;

  beforeAll(async () => {
    const testConnection = await createConnection({
      type: 'sqljs',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      logging: false,
      dropSchema: true,
      synchronize: true,
    });
    const communitiesRepository = testConnection.getRepository(Community);
    const communityUserStatusRepository = testConnection.getRepository(
      CommunityUserStatus,
    );
    const tasksRepository = testConnection.getRepository(Task);
    const pollsRepository = testConnection.getRepository(Poll);
    const projectsRepository = testConnection.getRepository(Project);
    const optionsRepository = testConnection.getRepository(Option);
    const usersRepository = testConnection.getRepository(User);
    const votesRepository = testConnection.getRepository(Vote);
    const sessionsRepository = testConnection.getRepository(Session);
    const messagesRepository = testConnection.getRepository(Message);

    communitiesModule = await Test.createTestingModule({
      controllers: [CommunitiesController],
      providers: [
        {
          provide: getRepositoryToken(CommunityUserStatus),
          useValue: communityUserStatusRepository,
        },
        MailerService,
        TasksService,
        {
          provide: getRepositoryToken(Task),
          useValue: tasksRepository,
        },
        ...web3Providers,
        CommunitiesService,
        {
          provide: getRepositoryToken(Community),
          useValue: communitiesRepository,
        },
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        PollsService,
        {
          provide: getRepositoryToken(Poll),
          useValue: pollsRepository,
        },
        ProjectsService,
        {
          provide: getRepositoryToken(Project),
          useValue: projectsRepository,
        },
        OptionsService,
        {
          provide: getRepositoryToken(Option),
          useValue: optionsRepository,
        },
        VotesService,
        {
          provide: getRepositoryToken(Vote),
          useValue: votesRepository,
        },
        SessionsService,
        {
          provide: getRepositoryToken(Session),
          useValue: sessionsRepository,
        },
        MessagesService,
        {
          provide: getRepositoryToken(Message),
          useValue: messagesRepository,
        },
        S3uploaderService,
      ],
    }).compile();

    communitiesController = communitiesModule.get<CommunitiesController>(
      CommunitiesController,
    );
    communitiesService = communitiesModule.get<CommunitiesService>(
      CommunitiesService,
    );
    mailerService = communitiesModule.get<MailerService>(MailerService);
    tasksService = communitiesModule.get<TasksService>(TasksService);
    usersService = communitiesModule.get<UsersService>(UsersService);
    s3uploaderService = communitiesModule.get<S3uploaderService>(
      S3uploaderService,
    );
  });

  describe('communities', () => {
    let community;
    let user;

    beforeAll(async () => {
      community = await communitiesService.createAndSave(newCommunity);
      await usersService.createAndSave(newUser);
      user = await usersService.findOneByAddress(newUser.address);
      user.curatorOf.push(community);
      await usersService.save(user);
      session.user = user;
    });

    it('should create and save a community', async () => {
      const result = await communitiesController.create(newCommunity);
      expect(result.id).toBe(2);
    });

    it('should get newly created communities', async () => {
      const result = await communitiesController.findAll();
      expect(result[0].name).toBe(newCommunity.name);
    });

    it('should return a member count', async () => {
      const result = await communitiesController.findAll();
      expect(result[0].memberCount).toBe(0);
    });

    it('should return an array of members for a community', async () => {
      await usersService.createAndSave(newUser2);
      const user2 = await usersService.findOneByAddress(newUser2.address);
      user2.memberOf.push(community);
      await usersService.save(user2);
      const result = await communitiesController.findMembersByCommunity(
        community.id,
        session,
      );
      expect(result[0].address).toBe(newUser2.address);
      expect(result[0].alias).toBe(null);
      expect(result.length).toBe(1);
    });

    it('should update a community', async () => {
      expect(community.name).toBe(newCommunity.name);
      community.name = 'new name but the game the same';
      const result = await communitiesController.update(
        community.id,
        { ...community },
        session,
      );
      expect(result.name).toBe('new name but the game the same');
    });

    it('should return an array of members for a community', async () => {
      await usersService.createAndSave(newUser2);
      const user2 = await usersService.findOneByAddress(newUser2.address);
      user2.memberOf.push(community);
      await usersService.save(user2);
      const result = await communitiesController.findMembersByCommunity(
        community.id,
        session,
      );
      expect(result[0].address).toBe(newUser2.address);
      expect(result[0].alias).toBe(null);
      expect(result.length).toBe(1);
    });

    it('should update the community to private and unprivate', async () => {
      const thisCommunity = await communitiesService.createAndSave(
        newCommunity,
      );
      community = await communitiesService.findOne(thisCommunity.id);
      community = await communitiesService.updateAndSaveCommunityPrivacy(
        community.id,
        false,
        false,
      );

      expect(community.isPrivate).toBe(false);
      community = await communitiesService.updateAndSaveCommunityPrivacy(
        community.id,
        true,
        false,
      );
      expect(community.isPrivate).toBe(true);
    });

    it('should blacklist and unblacklist a user', async () => {
      const thisCommunity = await communitiesService.createAndSave(
        newCommunity,
      );
      const thisUser = await usersService.createAndSave(newUser);
      thisUser.memberOf = [thisCommunity];
      await usersService.save(thisUser);
      const foundUser = await usersService.findOneByAddress(thisUser.address);
      await communitiesService.addBlacklistUser(foundUser.id, thisCommunity.id);
      let isBlacklisted = await communitiesService.isBlacklisted(
        foundUser.id,
        thisCommunity.id,
      );
      expect(isBlacklisted).toBe(true);
      await communitiesService.removeBlacklistUser(
        foundUser.id,
        thisCommunity.id,
      );
      isBlacklisted = await communitiesService.isBlacklisted(
        foundUser.id,
        thisCommunity.id,
      );
      expect(isBlacklisted).toBe(false);
    });
    it('should request access to a community', async () => {
      const thisCommunity = await communitiesService.createAndSave(
        newCommunity,
      );
      const thisUser = await usersService.createAndSave(newUser);

      const accessRequest = await communitiesService.requestAccess(
        thisCommunity.id,
        thisUser,
        'Some optional message',
      );
      // TODO: set this test case when we link up with mailer branch
      expect(accessRequest).toBe(true);
    });
  });

  it('should create the communityUserStatus entry for a user and then update the status', async () => {
    const thisUser = await usersService.createAndSave(newUser);
    const thisCommunity = await communitiesService.createAndSave(newCommunity);

    await communitiesService.createUserStatus(
      thisCommunity.id,
      thisUser.id,
      '',
    );
    const userStatus = await communitiesService.updateUserStatus(
      thisCommunity.id,
      thisUser.id,
      'pending',
    );
    expect(userStatus.communityId).toBe(6);
    expect(userStatus.userId).toBe(6);
    expect(userStatus.userStatus).toBe('pending');
  });

  describe('preapproving userStatus', () => {
    it('should create a user and create a userstatus', async () => {
      const thisCommunity = await communitiesService.createAndSave(
        newCommunity,
      );
      const user = await communitiesService.preapproveUserStatus(
        thisCommunity.id,
        newUser.address,
      );
      const userStatus = await communitiesService.getCommunityUserStatus(
        thisCommunity.id,
        user.id,
      );
      expect(userStatus.userStatus).toBe('approved');
    });
    it('should find a user and create a userstatus', async () => {
      const thisCommunity = await communitiesService.createAndSave(
        newCommunity,
      );
      const thisUser = await usersService.createAndSave(newUser);
      const user = await communitiesService.preapproveUserStatus(
        thisCommunity.id,
        newUser.address,
      );
      const userStatus = await communitiesService.getCommunityUserStatus(
        thisCommunity.id,
        user.id,
      );
      expect(userStatus.userStatus).toBe('approved');
    });
  });
});
