export class CommunityDto {
  name: string;
  address: string;
  tokenAddress: string;
  loggerAddress: string;
  image: string;
  icon: string;
  location: string;
  subtitle: string;
  dataImage: string;
  isPrivate: boolean;
  communityIntro: string;
  communityPurpose: string;
  membershipBenefits: string;
  votingPolicy: string;
  revenueDistributionPolicy: string;
  socialMediaLinks: string;
  telegramLink: string;
  curatorInfo: string;
  curatorEmail: string;
  quorum: number;
  memberCount: number;
}
