import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommunitiesController } from './communities.controller';
import { CommunitiesService } from './communities.service';
import { communitiesProviders } from './communities.providers';
import { usersProviders } from '../users/users.providers';
import { communityUserStatusProviders } from '../communityUserStatus/communityUserStatus.providers';
import { TasksModule } from '../tasks/tasks.module';
import { PollsModule } from '../polls/polls.module';
import { ProjectsModule } from '../projects/projects.module';
import { UsersModule } from '../users/users.module';
import { Community } from './community.entity';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';
import { VotesModule } from '../votes/votes.module';
import { S3uploaderModule } from '../s3uploader/s3uploader.module';
import { User } from '../users/user.entity';
import { MailerModule } from '../mailer/mailer.module';

@Module({
  imports: [
    PollsModule,
    TasksModule,
    ProjectsModule,
    UsersModule,
    VotesModule,
    VotesModule,
    S3uploaderModule,
    MailerModule,
    TypeOrmModule.forFeature([Community]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([CommunityUserStatus]),
  ],
  controllers: [CommunitiesController],
  providers: [
    CommunitiesService,
    ...communitiesProviders,
    ...usersProviders,
    ...communityUserStatusProviders,
  ],
  exports: [CommunitiesService],
})
export class CommunitiesModule {}
