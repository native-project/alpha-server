import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
  ManyToMany,
} from 'typeorm';
import { Task } from '../tasks/task.entity';
import { Project } from '../projects/project.entity';
import { Poll } from '../polls/poll.entity';
import { User } from '../users/user.entity';

@Entity()
export class Community {
  @PrimaryGeneratedColumn() id: number;

  @Column() name: string;

  @Column() address: string;

  @Column() tokenAddress: string;

  @Column() loggerAddress: string;

  @Column() image: string;

  @Column() icon: string;

  @Column() location: string;

  @Column() subtitle: string;

  @Column() dataImage: string;

  @Column() communityIntro: string;

  @Column() communityPurpose: string;

  @Column({ type: 'boolean', nullable: true, default: false })
  isPrivate: boolean;

  @Column({ type: 'varchar', nullable: true })
  membershipBenefits: string;

  @Column({ type: 'varchar', nullable: true })
  votingPolicy: string;

  @Column({ type: 'varchar', nullable: true })
  revenueDistributionPolicy: string;

  @Column({ type: 'varchar', nullable: true })
  socialMediaLinks: string;

  @Column({ type: 'varchar', nullable: true })
  telegramLink: string;

  @Column({ type: 'varchar', nullable: true })
  curatorInfo: string;

  @Column({ type: 'varchar', nullable: true })
  curatorEmail: string;

  @Column() quorum: number;

  @Column() support: number;

  @Column({ nullable: true })
  memberCount: number;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;

  @OneToMany((type) => Task, (task) => task.community)
  tasks: Task[];

  @OneToMany((type) => Project, (project) => project.community)
  projects: Project[];

  @OneToMany((type) => Poll, (poll) => poll.community)
  polls: Poll[];

  @ManyToMany((type) => User, (user) => user.memberOf)
  members: User[];

  @ManyToMany((type) => User, (user) => user.curatorOf)
  curators: User[];

  @ManyToMany((type) => User, (user) => user.blacklistedFrom)
  blacklisted: User[];
}
