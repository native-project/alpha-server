import {
  Body,
  Controller,
  Get,
  Post,
  Put,
  Param,
  Session,
  UseGuards,
} from '@nestjs/common';
import { CommunitiesService } from './communities.service';
import { TasksService } from '../tasks/tasks.service';
import { PollsService } from '../polls/polls.service';
import { ProjectsService } from '../projects/projects.service';
import { CommunityDto } from './community.dto';
import { Poll } from '../polls/poll.entity';
import { PollWithVoterDto } from '../polls/pollWithVoter.dto';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { UsersService } from '../users/users.service';
import {
  authenticatedSessionUser,
  permissionedCurator,
} from '../users/permission.helpers';

@Controller('communities')
@UseGuards(RolesGuard)
export class CommunitiesController {
  constructor(
    private readonly communitiesService: CommunitiesService,
    private readonly tasksService: TasksService,
    private readonly pollsService: PollsService,
    private readonly projectsService: ProjectsService,
    private readonly usersService: UsersService,
  ) {}

  @Get(':id')
  async findOne(@Param() params) {
    return await this.communitiesService.findOne(params.id);
  }

  @Get(':id/tasks')
  async findTasksByCommunity(@Param() params) {
    return await this.tasksService.findAllByCommunity(params.id);
  }

  @Get(':id/polls')
  async findPollsByCommunity(
    @Param() params,
    @Session() session,
  ): Promise<Poll[] | PollWithVoterDto[]> {
    if (!session.user) {
      return await this.pollsService.findAllByCommunity(params.id);
    }
    const polls = await this.pollsService.findAllByCommunityWithVoters(
      params.id,
    );
    const sessionUser = session.user;
    const user = await this.usersService.findOneByAddress(sessionUser.address);
    return this.pollsService.addVoterInfo(polls, user);
  }

  @Get(':id/members')
  @Roles('curator', 'admin')
  async findMembersByCommunity(@Param('id') communityId, @Session() session) {
    await permissionedCurator(session, communityId, this.usersService);

    return await this.communitiesService.findAllMembersOfCommunity(
      +communityId,
    );
  }

  @Get(':id/projects')
  async findProjectsByCommunity(@Param() params) {
    return await this.projectsService.findAllByCommunity(params.id);
  }

  @Get()
  async findAll() {
    return await this.communitiesService.findAll();
  }

  @Put(':id')
  @Roles('curator', 'admin')
  async update(@Param('id') communityId, @Body() body, @Session() session) {
    const community = await this.communitiesService.findOne(communityId);
    body.communityId = communityId;

    await permissionedCurator(session, communityId, this.usersService);

    if (community.isPrivate !== body.isPrivate) {
      await this.communitiesService.updateAndSaveCommunityPrivacy(
        community.id,
        body.isPrivate,
        body.blacklistAll,
      );
    }

    return await this.communitiesService.updateAndSaveCommunity(body);
  }

  @Post()
  @Roles('admin')
  async create(@Body() community: CommunityDto) {
    return await this.communitiesService.createAndSave(community);
  }

  @Post(':id/requestAccess')
  async requestAccess(
    @Param('id') communityId,
    @Body() body,
    @Session() session,
  ) {
    const { description } = body;
    const sessionUser = authenticatedSessionUser(session);
    return await this.communitiesService.requestAccess(
      communityId,
      sessionUser,
      description,
    );
  }

  @Post(':id/updateUserStatus')
  @Roles('curator', 'admin')
  async updateUserStatus(
    @Param('id') communityId,
    @Body() body,
    @Session() session,
  ) {
    const { userId, status } = body;
    await permissionedCurator(session, communityId, this.usersService);
    return await this.communitiesService.updateUserStatus(
      communityId,
      userId,
      status,
    );
  }

  @Post(':id/preapproveUser')
  @Roles('curator', 'admin')
  async preapproveUser(@Body() body, @Session() session) {
    const { communityId, address } = body;
    await permissionedCurator(session, communityId, this.usersService);
    return await this.communitiesService.preapproveUserStatus(
      communityId,
      address,
    );
  }
}
