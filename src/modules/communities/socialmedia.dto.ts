import { SocialLinks } from './sociallinks.enum';

export class SocialMedia {
  name: SocialLinks;
  link: string;
}
