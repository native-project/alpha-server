import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Community } from './community.entity';
import { User } from '../users/user.entity';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';
import { CommunityDto } from './community.dto';
import { communityUserStatus } from '../communityUserStatus/communityUserStatus.enum';
import { validate } from 'class-validator';
import { S3uploaderService } from '../s3uploader/s3uploader.service';
import { MailerService } from '../mailer/mailer.service';

@Injectable()
export class CommunitiesService {
  constructor(
    @InjectRepository(Community)
    private readonly communitiesRepository: Repository<Community>,
    @InjectRepository(CommunityUserStatus)
    private readonly communityUserStatusRepository: Repository<
      CommunityUserStatus
    >,
    private mailerService: MailerService,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private readonly s3uploaderService: S3uploaderService,
  ) {}

  async findOne(id): Promise<Community> {
    const community = await this.communitiesRepository.findOne(id, {
      relations: ['members'],
    });
    community.memberCount = community.members.length;
    return this.populateProperties(community);
  }

  async findOneWithUsers(id): Promise<Community> {
    const community = await this.communitiesRepository.findOne(id, {
      relations: ['members', 'curators', 'blacklisted'],
    });
    community.memberCount = community.members.length;
    return this.populateProperties(community);
  }

  async findOneByAddress(address): Promise<Community | null> {
    const communities = await this.communitiesRepository.find({
      where: { address },
      relations: ['members', 'curators'],
    });
    const community = communities ? communities[0] : null;
    community.memberCount = community.members.length;
    return this.populateProperties(community);
  }

  async findAllMembersOfCommunity(communityId): Promise<User[]> {
    const community = await this.communitiesRepository.findOne(communityId, {
      relations: ['members', 'curators', 'blacklisted'],
    });

    const allUserStatus = await this.getAllCommunityUserStatus(communityId);
    const blacklist = community.blacklisted;
    const members = community.members;
    await Promise.all(
      allUserStatus.map(async (userStatus) => {
        const user = await this.userRepository.findOne(userStatus.userId);
        user.userStatus = userStatus.userStatus;
        members.push(user);
      }),
    );
    await Promise.all(
      members.map(async (member) => {
        const blacklisted = blacklist.find((user) => member.id === user.id);
        const pendingUserStatus = member.userStatus ? member.userStatus : null;
        member.userStatus = blacklisted
          ? communityUserStatus.Blacklist
          : pendingUserStatus
            ? pendingUserStatus
            : communityUserStatus.Member;
      }),
    );
    return members;
  }

  async findAll(): Promise<Community[]> {
    const communities = await this.communitiesRepository.find({
      relations: ['members', 'blacklisted'],
    });
    communities.forEach((community) => {
      community = this.populateProperties(community);
      community.memberCount = community.members.length;
      delete community.members;
    });
    return communities;
  }

  async createAndSave(communityDto: CommunityDto): Promise<Community> {
    const errors = await validate(communityDto);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const community: Community = this.create(communityDto);
    return await this.communitiesRepository.save(community);
  }

  async updateAndSaveCommunity(communityUpdate: Community): Promise<Community> {
    const errors = await validate(communityUpdate);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    communityUpdate.membershipBenefits = JSON.stringify(
      communityUpdate.membershipBenefits,
    );
    communityUpdate.socialMediaLinks = JSON.stringify(
      communityUpdate.socialMediaLinks,
    );
    const result = await this.communitiesRepository.save(communityUpdate);
    return this.populateProperties(result);
  }

  create(communityDto: CommunityDto): Community {
    communityDto.membershipBenefits = JSON.stringify(
      communityDto.membershipBenefits,
    );
    communityDto.socialMediaLinks = JSON.stringify(
      communityDto.socialMediaLinks,
    );
    const result = this.communitiesRepository.create(communityDto);
    return this.populateProperties(result);
  }

  populateProperties(community: Community) {
    community.membershipBenefits = JSON.parse(
      community.membershipBenefits || null,
    );
    community.socialMediaLinks = JSON.parse(community.socialMediaLinks || null);
    return community;
  }

  async addBlacklistUser(
    userId: number,
    communityId: number,
  ): Promise<Community> {
    const community = await this.communitiesRepository.findOne(communityId, {
      relations: ['blacklisted'],
    });
    const user = await this.userRepository.findOne(userId);

    const blacklisted = community.blacklisted;
    blacklisted.push(user);
    community.blacklisted = blacklisted;
    try {
      return await this.communitiesRepository.save(community);
    } catch (error) {
      return error;
    }
  }

  async removeBlacklistUser(
    userId: number,
    communityId: number,
  ): Promise<Community> {
    const community = await this.communitiesRepository.findOne(communityId, {
      relations: ['blacklisted'],
    });
    const blacklisted = community.blacklisted;
    blacklisted.splice(blacklisted.findIndex((e) => e.id === userId), 1);

    community.blacklisted = blacklisted;
    try {
      return await this.communitiesRepository.save(community);
    } catch (error) {
      return error;
    }
  }

  async isBlacklisted(userId: number, communityId: number): Promise<boolean> {
    const community = await this.communitiesRepository.findOne(communityId, {
      relations: ['blacklisted'],
    });
    const blacklisted = community.blacklisted;
    const blacklist = blacklisted.splice(
      blacklisted.findIndex((e) => e.id === userId),
      1,
    );
    if (blacklist.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  async requestAccess(communityId: number, user: User, message: string) {
    const community = await this.communitiesRepository.findOne(communityId);
    await this.updateUserStatus(
      communityId,
      user.id,
      communityUserStatus.Pending,
    );
    try {
      await this.mailerService.sendRequestAccessEmail(
        community.curatorEmail,
        user,
        message,
      );
      return true;
    } catch (error) {
      return error;
    }
  }

  async createUserStatus(
    communityId: number,
    userId: number,
    status: string,
  ): Promise<any> {
    const userStatus = this.communityUserStatusRepository.create();
    userStatus.userId = userId;
    userStatus.communityId = communityId;
    userStatus.userStatus = status;
    return await this.communityUserStatusRepository.save(userStatus);
  }

  async updateUserStatus(
    communityId: number,
    userId: number,
    status: string,
  ): Promise<any> {
    if (typeof status !== 'string') {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `Status is not a string`,
        },
        400,
      );
    }
    switch (status) {
      case communityUserStatus.Pending:
      case communityUserStatus.Denied:
      case communityUserStatus.Approved:
        let userStatus = await this.communityUserStatusRepository.findOne({
          userId,
          communityId,
        });
        if (userStatus === undefined) {
          userStatus = this.communityUserStatusRepository.create();
          userStatus = { userId, communityId, userStatus: status };
        } else {
          userStatus.userStatus = status;
        }
        return await this.communityUserStatusRepository.save(userStatus);
      case communityUserStatus.Blacklist:
        return this.addBlacklistUser(userId, communityId);
      case communityUserStatus.Member:
        return this.removeBlacklistUser(userId, communityId);
      default:
        throw new HttpException(
          {
            status: HttpStatus.BAD_REQUEST,
            error: `Invalid Status`,
          },
          400,
        );
    }
  }

  async getCommunityUserStatus(
    communityId: number,
    userId: number,
  ): Promise<any> {
    if (typeof communityId !== 'number' && typeof userId !== 'number') {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `Parameters must be numbers`,
        },
        400,
      );
    }
    const userStatus = await this.communityUserStatusRepository.findOne({
      userId,
      communityId,
    });
    return userStatus;
  }

  async getAllCommunityUserStatus(communityId: number): Promise<any> {
    if (typeof communityId !== 'number') {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `Parameters must be numbers`,
        },
        400,
      );
    }
    const allUserStatus = await this.communityUserStatusRepository.find({
      communityId,
    });
    return allUserStatus;
  }

  async updateAndSaveCommunityPrivacy(
    communityId: number,
    isPrivate: boolean,
    blacklistAll: boolean,
  ): Promise<Community> {
    const errors = await validate(isPrivate);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }

    const community = await this.findOneWithUsers(communityId);

    if (community.isPrivate !== isPrivate) {
      if (blacklistAll) {
        const users = community.members;
        const blacklisted = community.blacklisted;
        users.map(async (user) => {
          blacklisted.push(user);
        });
        community.blacklisted = blacklisted;
      }
    }
    community.isPrivate = isPrivate;
    return await this.communitiesRepository.save(community);
  }

  async preapproveUserStatus(
    communityId: number,
    address: string,
  ): Promise<any> {
    const user = await this.userRepository.findOne({ address });
    if (user) {
      await this.updateUserStatus(
        communityId,
        user.id,
        communityUserStatus.Approved,
      );
      return await this.userRepository.findOne(user.id);
    } else {
      const userDto = {
        address,
      };
      const createdUser = await this.userRepository.create(userDto);
      await this.userRepository.save(createdUser);
      const savedUser = await this.userRepository.findOne(createdUser.id);
      await this.createUserStatus(
        communityId,
        savedUser.id,
        communityUserStatus.Approved,
      );
      return await this.userRepository.findOne(savedUser.id);
    }
  }
}
