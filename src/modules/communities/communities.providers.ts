import { Connection } from 'typeorm';
import { Community } from './community.entity';

export const communitiesProviders = [
  {
    provide: 'CommunitiesRepository',
    useFactory: (connection: Connection) => connection.getRepository(Community),
    inject: [Connection],
  },
];
