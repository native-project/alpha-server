import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { validate } from 'class-validator';

import { Session } from './session.entity';

@Injectable()
export class SessionsService {
  constructor(
    @InjectRepository(Session)
    private readonly sessionsRepository: Repository<Session>,
  ) {}

  async createAndSave(sessionDto): Promise<Session> {
    const errors = await validate(sessionDto);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const session: Session = this.create(sessionDto);
    return await this.sessionsRepository.save(session);
  }

  create(sessionDto: Session): Session {
    return this.sessionsRepository.create(sessionDto);
  }
  async findOne(id): Promise<Session> {
    return await this.sessionsRepository.findOne(id, {
      relations: [],
    });
  }

  async findOneByUser(user): Promise<Session> {
    const userString = JSON.stringify(user.address);
    return await this.sessionsRepository.findOne({
      where: {
        user: Like('%' + `${userString}` + '%'),
      },
    });
  }

  public async resetSession(user) {
    const userString = JSON.stringify(user.address);
    const sessions = await this.sessionsRepository.find({
      where: {
        user: Like('%' + `${userString}` + '%'),
      },
    });
    const sessionToReset = sessions[sessions.length - 1];
    const json = JSON.parse(sessionToReset.json);
    json.user = user;
    const jsonString = JSON.stringify(json);
    sessionToReset.json = jsonString;
    return await this.sessionsRepository.save(sessionToReset);
  }
}
