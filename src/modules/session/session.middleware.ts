import session from 'express-session';
import { Injectable, NestMiddleware } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeormStore } from 'connect-typeorm';
import { Repository } from 'typeorm';
import { Session } from './session.entity';

@Injectable()
export class SessionMiddleware implements NestMiddleware {
  constructor(
    @InjectRepository(Session)
    private readonly sessionRepository: Repository<Session>,
  ) {}

  resolve() {
    // TODO
    const secureCookie = process.env.SESSION_COOKIE_SECURE === 'true';
    return session({
      store: new TypeormStore({
        ttl: parseInt(process.env.SESSION_TTL, 10),
      }).connect(this.sessionRepository),
      secret: process.env.SESSION_SECRET,
      name: process.env.APP_NAME,
      resave: false,
      saveUninitialized: false,
    });
  }
}
