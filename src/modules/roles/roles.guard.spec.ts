import { applyRoles } from './roles.guard';
import { Roles } from './roles.enum';
import httpMocks from 'node-mocks-http';
import {
  existingCurator,
  getSession,
} from '../../common/helpers/test/fixtures';

function convertRolesEnum(rolesEnum) {
  const roles = [];
  for (const key in rolesEnum) {
    if (rolesEnum.hasOwnProperty) {
      roles.push(Roles[key]);
    }
  }
  return roles;
}

describe('RolesGuard', () => {
  const request = httpMocks.createRequest({
    method: 'GET',
    url: '/task',
  });
  const session = getSession(request, existingCurator.user);
  it('should not allow access if no session', async () => {
    const roles = convertRolesEnum(Roles);
    const result = await applyRoles(roles, request);
    expect(result).toBe(false);
  });

  it('should allow curator user when roles include', async () => {
    const roles = convertRolesEnum(Roles);
    request.session = session;
    const result = await applyRoles(roles, request);
    expect(result).toBe(true);
  });

  it('should deny curator user when roles are only [admin]', async () => {
    const roles = ['admin'];
    const result = await applyRoles(roles, request);
    expect(result).toBe(false);
  });
});
