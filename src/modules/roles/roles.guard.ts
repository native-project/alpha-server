import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    const request = context.switchToHttp().getRequest();
    return applyRoles(roles, request);
  }
}

export const applyRoles = (roles, request) => {
  if (!roles) {
    return true;
  }
  if (!request.session) {
    return false;
  }
  const user = request.session.user;
  if (!user) {
    return false;
  }
  const hasRole = roles.indexOf(user.role) > -1;
  return user.role && hasRole;
};
