export enum Roles {
  Admin = 'admin',
  Curator = 'curator',
  Member = 'member',
}
