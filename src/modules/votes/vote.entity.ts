import {
  Entity,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Poll } from '../polls/poll.entity';
import { User } from '../users/user.entity';
import { Option } from '../polls/options/option.entity';
import { Validate } from 'class-validator';
import { CategoryName } from './categoryName.validator';
import { Categories } from './votes.enum';

@Entity()
export class Vote {
  @PrimaryGeneratedColumn() id: number;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;

  @Column({ type: 'varchar', nullable: true })
  @Validate(CategoryName)
  category: Categories;

  @ManyToOne((type) => Poll, (poll) => poll.votes, {
    eager: false,
  })
  poll: Poll;

  @ManyToOne((type) => Option, (option) => option.votes, {
    eager: false,
  })
  option: Option;

  @ManyToOne((type) => User, (user) => user.votes, {
    eager: false,
  })
  user: User;
}
