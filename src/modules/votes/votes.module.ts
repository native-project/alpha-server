import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { votesProviders } from './votes.provider';
import { VotesService } from './votes.service';
import { Vote } from './vote.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Vote])],
  providers: [VotesService, ...votesProviders],
  exports: [VotesService],
})
export class VotesModule {}
