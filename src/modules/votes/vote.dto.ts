import { User } from '../users/user.entity';
import { Poll } from '../polls/poll.entity';
import { Option } from '../polls/options/option.entity';

export class VoteDto {
  user: User;
  option: Option;
  poll: Poll;
}
