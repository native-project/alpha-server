import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'categoryName', async: false })
export class CategoryName implements ValidatorConstraintInterface {
  validNames: string[] = ['poll', 'project'];

  validate(name: string, args: ValidationArguments) {
    return this.validNames.indexOf(name) >= 0;
  }

  defaultMessage(args: ValidationArguments) {
    return `The category name must be ${this.validNames.join(', ')}`;
  }
}
