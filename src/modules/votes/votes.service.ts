import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Vote } from './vote.entity';

@Injectable()
export class VotesService {
  constructor(
    @InjectRepository(Vote) private readonly votesRepository: Repository<Vote>,
  ) {}

  async findAllByPoll(pollId): Promise<Vote[]> {
    return await this.votesRepository.find({
      where: {
        pollId,
        relations: ['option'],
      },
    });
  }

  async findAllByUser(userId): Promise<Vote[]> {
    return await this.votesRepository.find({
      where: {
        userId,
        relations: ['option'],
      },
    });
  }
}
