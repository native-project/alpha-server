import { Connection } from 'typeorm';
import { Vote } from './vote.entity';

export const votesProviders = [
  {
    provide: 'VotesRepository',
    useFactory: (connection: Connection) => connection.getRepository(Vote),
    inject: [Connection],
  },
];
