import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { Community } from '../communities/community.entity';
import { ProjectDto } from './project.dto';
import { validate } from 'class-validator';
import { Project } from './project.entity';
import { ProjectStatuses } from './projectStatuses.enum';
import { generateContractId } from '../../common/helpers/utils/utils';
import { PollsService } from '../polls/polls.service';
import moment from 'moment';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private readonly projectsRepository: Repository<Project>,
    @InjectRepository(Community)
    private readonly communitiesRepository: Repository<Community>,
    private readonly pollsService: PollsService,
  ) {}

  onlyEditable(putData) {
    const readonly = this.projectsRepository.metadata.ownColumns.filter(
      (item) => item.isReadonly,
    );
    Object.keys(putData).forEach((key) => {
      if (readonly.find((item) => item.propertyName === key)) {
        delete putData[key];
      }
    });
    return putData;
  }

  async findOne(id): Promise<Project> {
    const project = await this.projectsRepository.findOne(id, {
      relations: ['polls'],
    });
    project.pollHasVotes = await this.pollHasVotes(project);
    return project;
  }

  async findAllByCommunity(communityId): Promise<Project[]> {
    const community: Community = await this.communitiesRepository.findOne(
      communityId,
    );

    let projects = await this.projectsRepository.find({
      where: {
        community,
      },
      relations: ['polls'],
    });
    projects = await this.updateClosedStatuses(projects, community);
    projects = await this.updateVoteStatus(projects);
    return projects;
  }

  async createAndSave(projectDto: ProjectDto): Promise<Project> {
    const project: Project = this.create(projectDto);
    const errors = await validate(project);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }

    const community: Community = await this.communitiesRepository.findOne(
      projectDto.communityId,
    );
    if (community === undefined) {
      throw new HttpException('Invalid community', HttpStatus.BAD_REQUEST);
    }
    project.community = community;

    project.contractId = await generateContractId(
      project,
      this.projectsRepository,
    );

    const savedProject = await this.projectsRepository.save(project);
    project.polls = [await this.pollsService.createAndSaveForProject(project)];

    return savedProject;
  }

  async updateAndSave(projectId: number, put: ProjectDto): Promise<Project> {
    // remove readonly fields
    put = this.onlyEditable(put);

    const updates: Project = this.create(put);
    const errors = await validate(updates, { skipMissingProperties: true });
    const project: Project = await this.projectsRepository.findOne(projectId);

    if (project === undefined) {
      throw new HttpException('Invalid project', HttpStatus.BAD_REQUEST);
    }

    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    } else {
      await this.update(projectId, put);
      return await this.projectsRepository.findOne(projectId);
    }
  }

  async updateStatus(projectId: string, status: string): Promise<any> {
    const project: Project = await this.projectsRepository.findOne({
      id: projectId,
    });
    if (!Object.values(ProjectStatuses).includes(status)) {
      throw new HttpException(
        'Status not a valid enum',
        HttpStatus.BAD_REQUEST,
      );
    }
    project.status = status;

    return this.projectsRepository.save(project);
  }

  async updateStatusByContractId(
    contractId: string,
    status: string,
  ): Promise<any> {
    const project: Project = await this.projectsRepository.findOne({
      contractId,
    });
    project.status = status;
    return this.projectsRepository.save(project);
  }

  create(projectDto: ProjectDto): Project {
    return this.projectsRepository.create(projectDto);
  }

  update(projectId: number, projectDto: ProjectDto): Promise<UpdateResult> {
    return this.projectsRepository.update(projectId, projectDto);
  }

  async updateClosedStatuses(
    projects: Project[],
    community: Community,
  ): Promise<Project[]> {
    const now = moment();
    return await Promise.all(
      projects.map(
        async (project): Promise<Project> => {
          const endDate = moment(project && project.endDate);
          const isPollOpen = now.isBefore(endDate);
          let willPass;

          if (
            !isPollOpen &&
            project.status !== ProjectStatuses.Funded &&
            project.status !== ProjectStatuses.Canceled &&
            project.polls.length > 0
          ) {
            const poll = await this.pollsService.findOneWithVoters(
              project.polls[0].id,
            );
            if (poll) {
              willPass = await this.pollsService.checkIfWillPass(
                poll,
                community,
              );
              project.status = willPass
                ? ProjectStatuses.Approved
                : ProjectStatuses.Denied;
            }
          }
          return project;
        },
      ),
    );
  }
  async updateVoteStatus(projects: Project[]) {
    return await Promise.all(
      projects.map(async (project) => {
        project.pollHasVotes = await this.pollHasVotes(project);
        return project;
      }),
    );
  }

  async pollHasVotes(project: Project): Promise<boolean> {
    if (!project.polls || project.polls.length === 0) {
      return false;
    }
    return await this.pollsService.hasVotes(project.polls[0]);
  }
}
