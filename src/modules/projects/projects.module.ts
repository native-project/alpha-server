import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { projectsProviders } from './projects.providers';
import { communitiesProviders } from '../communities/communities.providers';
import { ProjectsService } from './projects.service';
import { ProjectsController } from './projects.controller';
import { Project } from './project.entity';
import { Community } from '../communities/community.entity';
import { PollsModule } from '../polls/polls.module';
import { PollsService } from '../polls/polls.service';
import { Poll } from '../polls/poll.entity';
import { Vote } from '../votes/vote.entity';
import { Option } from '../polls/options/option.entity';
import { User } from '../users/user.entity';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Project]),
    TypeOrmModule.forFeature([Community]),
    TypeOrmModule.forFeature([Poll]),
    TypeOrmModule.forFeature([Vote]),
    TypeOrmModule.forFeature([Option]),
    PollsModule,
    UsersModule,
  ],
  controllers: [ProjectsController],
  providers: [
    ProjectsService,
    PollsService,
    ...communitiesProviders,
    ...projectsProviders,
  ],
  exports: [ProjectsService],
})
export class ProjectsModule {}
