export enum ProjectStatuses {
  Initialized = 'initialized',
  Escrowed = 'escrowed',
  Denied = 'denied',
  Approved = 'approved',
  Canceled = 'canceled',
  PendingCompletion = 'pendingCompletion',
  Funded = 'funded',
}
