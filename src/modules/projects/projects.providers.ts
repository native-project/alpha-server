import { Connection } from 'typeorm';
import { Project } from './project.entity';

export const projectsProviders = [
  {
    provide: 'ProjectsRepository',
    useFactory: (connection: Connection) => connection.getRepository(Project),
    inject: [Connection],
  },
];
