import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseGuards,
  Put,
  Session,
} from '@nestjs/common';
import { ProjectsService } from './projects.service';
import { UsersService } from '../users/users.service';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { permissionedCurator } from '../users/permission.helpers';

@Controller('projects')
@UseGuards(RolesGuard)
export class ProjectsController {
  constructor(
    private readonly projectsService: ProjectsService,
    private readonly usersService: UsersService,
  ) {}

  @Get(':projectId')
  @Roles('curator', 'admin', 'member')
  async findOne(@Param('projectId') projectId) {
    return await this.projectsService.findOne(projectId);
  }

  @Post(':projectId/updateStatus')
  @Roles('curator', 'admin')
  async updateStatus(
    @Param('projectId') projectId,
    @Body() body,
    @Session() session,
  ) {
    const project = await this.projectsService.findOne(projectId);
    await permissionedCurator(session, project.community.id, this.usersService);
    const { status } = body;
    return await this.projectsService.updateStatus(projectId, status);
  }

  @Post()
  @Roles('curator', 'admin')
  async create(@Body() project, @Session() session) {
    await permissionedCurator(session, project.communityId, this.usersService);
    return await this.projectsService.createAndSave(project);
  }

  @Put(':projectId')
  @Roles('curator', 'admin')
  async update(
    @Param('projectId') projectId,
    @Body() project,
    @Session() session,
  ) {
    await permissionedCurator(session, project.community.id, this.usersService);

    return await this.projectsService.updateAndSave(projectId, project);
  }
}
