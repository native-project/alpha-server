export class ProjectDto {
  contractId?: string;
  communityId: number;
  title: string;
  subtitle: string;
  description: string;
  additionalInfo: string;
  totalCost: number;
  startDate: string;
  endDate: string;
  costBreakdownUrl: string;
  roadmapUrl: string;
  imageUrl: string;
  status: string;
  address: string;
}
