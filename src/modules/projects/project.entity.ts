import {
  Entity,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
import { Community } from '../communities/community.entity';

import { Length, IsDateString, IsInt, Min, IsOptional } from 'class-validator';
import { Poll } from '../polls/poll.entity';
import { ProjectStatuses } from './projectStatuses.enum';
@Entity()
export class Project {
  @PrimaryGeneratedColumn('uuid') id: string;

  @Column({ type: 'text', nullable: true })
  contractId: string;

  @ManyToOne((type) => Community, (community) => community.projects, {
    eager: true,
  })
  community: Community;

  @Column({ readonly: true })
  @Length(2, 128)
  title: string;

  @Column({ readonly: true })
  @Length(2, 128)
  subtitle: string;

  @Column({ type: 'text', readonly: true })
  description: string;

  @Column({ type: 'text', readonly: true, nullable: true })
  additionalInfo: string;

  @Column({ readonly: true })
  totalCost: number;

  @Column({ readonly: true })
  @IsDateString()
  startDate: Date;

  @Column({ readonly: true })
  @IsDateString()
  endDate: Date;

  @Column({ readonly: true, nullable: true })
  costBreakdownUrl: string;

  @Column({ readonly: true, nullable: true })
  roadmapUrl: string;

  @Column({ readonly: true, nullable: true, default: null })
  @IsOptional()
  imageUrl: string;

  @OneToMany((type) => Poll, (poll) => poll.project)
  polls: Poll[];

  @Column({ type: 'varchar', default: ProjectStatuses.Initialized })
  @IsOptional()
  status: string;

  @Column({ readonly: true })
  address: string;

  @CreateDateColumn({ readonly: true })
  createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;

  pollHasVotes: boolean;
}
