import { Test, TestingModule } from '@nestjs/testing';
import { ProjectsController } from './projects.controller';
import { ProjectsService } from './projects.service';
import { CommunitiesService } from '../communities/communities.service';
import { MailerService } from '../mailer/mailer.service';
import { createConnection } from 'typeorm';
import { Community } from '../communities/community.entity';
import { Project } from './project.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import {
  newProject,
  newCommunity,
  getSession,
  existingCurator,
  newUser,
} from '../../common/helpers/test/fixtures';
import { S3uploaderService } from '../s3uploader/s3uploader.service';
import { User } from '../users/user.entity';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';
import { ProjectStatuses } from './projectStatuses.enum';
import { PollsService } from '../polls/polls.service';
import { Poll } from '../polls/poll.entity';
import { Option } from '../polls/options/option.entity';
import { Vote } from '../votes/vote.entity';
import httpMocks from 'node-mocks-http';
import { UsersService } from '../users/users.service';
import { Message } from '../messages/message.entity';
import { Session } from '../session/session.entity';

describe('ProjectController', () => {
  let projectsModule: TestingModule;
  let projectsController: ProjectsController;
  let projectsService: ProjectsService;
  let communitiesService: CommunitiesService;
  let s3uploaderService: S3uploaderService;
  let usersService: UsersService;

  const request = httpMocks.createRequest({
    method: 'GET',
    url: '/projects',
  });
  const session = getSession(request);

  beforeAll(async () => {
    const testConnection = await createConnection({
      type: 'sqljs',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      logging: false,
      dropSchema: true,
      synchronize: true,
    });
    const communitiesRepository = testConnection.getRepository(Community);
    const usersRepository = testConnection.getRepository(User);
    const projectsRepository = testConnection.getRepository(Project);
    const messagesRepository = testConnection.getRepository(Message);
    const sessionsRepository = testConnection.getRepository(Session);
    const communityUserStatusRepository = testConnection.getRepository(
      CommunityUserStatus,
    );
    const pollsRepository = testConnection.getRepository(Poll);
    const optionsRepository = testConnection.getRepository(Option);
    const votesRepository = testConnection.getRepository(Vote);

    projectsModule = await Test.createTestingModule({
      controllers: [ProjectsController],
      providers: [
        {
          provide: getRepositoryToken(CommunityUserStatus),
          useValue: communityUserStatusRepository,
        },
        {
          provide: getRepositoryToken(Poll),
          useValue: pollsRepository,
        },
        {
          provide: getRepositoryToken(Option),
          useValue: optionsRepository,
        },
        {
          provide: getRepositoryToken(Vote),
          useValue: votesRepository,
        },
        {
          provide: getRepositoryToken(Project),
          useValue: projectsRepository,
        },
        {
          provide: getRepositoryToken(Community),
          useValue: communitiesRepository,
        },
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        {
          provide: getRepositoryToken(Message),
          useValue: messagesRepository,
        },
        {
          provide: getRepositoryToken(Session),
          useValue: sessionsRepository,
        },
        MailerService,
        ProjectsService,
        PollsService,
        CommunitiesService,
        S3uploaderService,
        UsersService,
      ],
    }).compile();

    projectsController = projectsModule.get<ProjectsController>(
      ProjectsController,
    );
    projectsService = projectsModule.get<ProjectsService>(ProjectsService);
    communitiesService = projectsModule.get<CommunitiesService>(
      CommunitiesService,
    );
    s3uploaderService = projectsModule.get<S3uploaderService>(
      S3uploaderService,
    );
    usersService = projectsModule.get<UsersService>(UsersService);
  });

  describe('projects', () => {
    let thisCommunity;
    let user;
    let projectCurator;

    beforeAll(async () => {
      thisCommunity = await communitiesService.createAndSave(newCommunity);
      user = await usersService.findOneByAddress(newUser.address);
      projectCurator = await usersService.createAndSave(existingCurator.user);
      projectCurator = await usersService.findOne(projectCurator.id);
      projectCurator.curatorOf.push(thisCommunity);
      await usersService.save(projectCurator);
    });

    it('should update the status of a project', async () => {
      session.user = projectCurator;
      const savedProject = await projectsController.create(newProject, session);
      const project = await projectsController.findOne(savedProject.id);

      expect(project.status).toBe('initialized');
      await projectsService.updateStatus(
        savedProject.id,
        ProjectStatuses.Denied,
      );
      const updatedProject = await projectsController.findOne(savedProject.id);
      expect(updatedProject.status).toBe('denied');
    });

    it('should update the status of a project using the contractid', async () => {
      const savedProject = await projectsController.create(newProject, session);
      const project = await projectsController.findOne(savedProject.id);
      await projectsService.updateStatusByContractId(
        savedProject.contractId,
        ProjectStatuses.Denied,
      );
      const updatedProject = await projectsController.findOne(savedProject.id);
      expect(updatedProject.status).toBe('denied');
    });

    it('should create and save a project', async () => {
      const result = await projectsController.create(newProject, session);
      expect(result.title).toBe(newProject.title);
    });

    it('should have an associated poll with approve and deny options', async () => {
      const result = await projectsController.create(newProject, session);
      expect(result.polls[0]).toHaveProperty('options');
      expect(result.polls[0].options[0].name).toBe('Approve');
      expect(result.polls[0].options[1].name).toBe('Deny');
    });

    it('should find a single project by id', async () => {
      const project = await projectsController.create(newProject, session);

      const results = await projectsController.findOne({
        projectId: 1,
      });

      expect(results.title).toBe(newProject.title);
    });

    it('should findAllProjects by community', async () => {
      const project1 = await projectsController.create(newProject, session);
      const project2 = await projectsController.create(newProject, session);
      const project3 = await projectsController.create(newProject, session);

      const results = await projectsService.findAllByCommunity({
        communityId: 1,
      });
      expect(results.length).toBeGreaterThanOrEqual(3);
    });

    it('should update a project', async () => {
      const result = await projectsController.create(newProject, session);
      expect(result.title).toBe(newProject.title);
      const project = await projectsController.findOne(result.id);

      const updatedProject = project;
      const updatedCostBreakdownUrl = 'http://costbreakdown.de';
      updatedProject.costBreakdownUrl = updatedCostBreakdownUrl;
      const updatedResult = await projectsController.update(
        result.id,
        updatedProject,
        session,
      );
      expect(updatedResult.costBreakdownUrl).toBe(updatedCostBreakdownUrl);
    });

    it('should find a single project by id', async () => {
      const project = await projectsController.create(newProject, session);

      const results = await projectsController.findOne({
        projectId: 1,
      });

      expect(results.title).toBe(newProject.title);
    });

    it('should findAllProjects by community', async () => {
      const project1 = await projectsController.create(newProject, session);
      const project2 = await projectsController.create(newProject, session);
      const project3 = await projectsController.create(newProject, session);

      const results = await projectsService.findAllByCommunity({
        communityId: 1,
      });
      expect(results.length).toBeGreaterThanOrEqual(3);
    });

    it('should update a project', async () => {
      const result = await projectsController.create(newProject, session);
      expect(result.title).toBe(newProject.title);
      const project = await projectsController.findOne(result.id);

      const updatedProject = project;
      const updatedCostBreakdownUrl = 'http://costbreakdown.de';
      updatedProject.costBreakdownUrl = updatedCostBreakdownUrl;
      const updatedResult = await projectsController.update(
        result.id,
        updatedProject,
        session,
      );
      expect(updatedResult.costBreakdownUrl).toBe(updatedCostBreakdownUrl);
    });

    it('should be associated with a community', async () => {
      const result = await projectsController.create(newProject, session);
      const thisProject = await projectsController.findOne(result.id);
      expect(thisProject.community.id).toBe(thisCommunity.id);
    });
  });
});
