export class ClaimRequest {
  taskId: string;
  email: string;
}

export class ConfirmRequest {
  taskId: string;
}

export class SubmitRequest {
  taskId: string;
}
