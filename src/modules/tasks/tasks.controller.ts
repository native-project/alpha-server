import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Session,
  UseGuards,
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { RolesGuard } from '../roles/roles.guard';
import { Roles } from '../roles/roles.decorator';
import { UsersService } from '../users/users.service';
import { TaskStatuses } from './taskStatuses.enum';
import {
  permissionedCurator,
  permissionedMember,
} from '../users/permission.helpers';

@Controller('tasks')
@UseGuards(RolesGuard)
export class TasksController {
  constructor(
    private readonly tasksService: TasksService,
    private readonly usersService: UsersService,
  ) {}

  @Get(':taskId')
  async findOne(@Param('taskId') taskId) {
    return await this.tasksService.findOne(taskId);
  }

  @Post('claim')
  @Roles('curator', 'member')
  async claim(@Body() body, @Session() session) {
    if (!body.taskId || !body.email) {
      throw new HttpException(
        'You must supply a taskId and email.',
        HttpStatus.BAD_REQUEST,
      );
    }
    const task = await this.tasksService.findOne(body.taskId);
    if (!task.community || !task.community.id) {
      throw new HttpException(
        'This task is not associated with a community.',
        HttpStatus.BAD_REQUEST,
      );
    }

    const sessionUser = await permissionedMember(
      session,
      +task.community.id,
      this.usersService,
    );

    return await this.tasksService.claim(body, sessionUser);
  }

  @Post('submit')
  @Roles('curator', 'member')
  async submit(@Body() body, @Session() session) {
    if (!body.taskId || !body.email) {
      throw new HttpException(
        'You must supply a taskId and email.',
        HttpStatus.BAD_REQUEST,
      );
    }

    const task = await this.tasksService.findOne(body.taskId);
    if (!task.community || !task.community.id) {
      throw new HttpException(
        'This task is not associated with a community.',
        HttpStatus.BAD_REQUEST,
      );
    }

    await permissionedMember(session, +task.community.id, this.usersService);

    return await this.tasksService.submitForApproval(body);
  }

  @Post('deny-claim')
  @Roles('curator', 'admin')
  async denyClaim(@Body() body, @Session() session) {
    if (!body.taskId) {
      throw new HttpException(
        'You must supply a taskId.',
        HttpStatus.BAD_REQUEST,
      );
    }

    const task = await this.tasksService.findOne(body.taskId);
    if (!task.community || !task.community.id) {
      throw new HttpException(
        'This task is not associated with a community.',
        HttpStatus.BAD_REQUEST,
      );
    }

    await permissionedCurator(session, +task.community.id, this.usersService);

    return this.tasksService.denyClaim(task);
  }

  @Post('deny-submission')
  @Roles('curator', 'admin')
  async denySubmission(@Body() body, @Session() session) {
    if (!body.taskId) {
      throw new HttpException(
        'You must supply a taskId.',
        HttpStatus.BAD_REQUEST,
      );
    }

    const task = await this.tasksService.findOne(body.taskId);
    if (!task.community || !task.community.id) {
      throw new HttpException(
        'This task is not associated with a community.',
        HttpStatus.BAD_REQUEST,
      );
    }

    await permissionedCurator(session, +task.community.id, this.usersService);

    return this.tasksService.denySubmission(task);
  }

  @Post('approve-submission')
  @Roles('curator', 'admin')
  async approveSubmission(@Body() body, @Session() session) {
    if (!body.taskId) {
      throw new HttpException(
        'You must supply a taskId.',
        HttpStatus.BAD_REQUEST,
      );
    }
    const task = await this.tasksService.findOne(body.taskId);
    if (!task.community || !task.community.id) {
      throw new HttpException(
        'This task is not associated with a community.',
        HttpStatus.BAD_REQUEST,
      );
    }
    await permissionedCurator(session, +task.community.id, this.usersService);

    return await this.tasksService.approveSubmission(body);
  }

  @Post('pending-cancellation')
  @Roles('curator', 'admin')
  async setPendingCancellation(@Body() body, @Session() session) {
    if (!body.taskId) {
      throw new HttpException(
        'You must supply a taskId.',
        HttpStatus.BAD_REQUEST,
      );
    }
    const task = await this.tasksService.findOne(body.taskId);
    if (!task.community || !task.community.id) {
      throw new HttpException(
        'This task is not associated with a community.',
        HttpStatus.BAD_REQUEST,
      );
    }
    await permissionedCurator(session, +task.community.id, this.usersService);

    return await this.tasksService.updateStatus(
      task,
      TaskStatuses.PendingCancellation,
    );
  }

  @Post()
  @Roles('curator', 'admin')
  async create(@Body() task, @Session() session) {
    await permissionedCurator(session, +task.communityId, this.usersService);

    return await this.tasksService.createAndSave(task);
  }
}
