import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { tasksProviders } from './tasks.providers';
import { communitiesProviders } from '../communities/communities.providers';
import { usersProviders } from '../users/users.providers';
import { TasksService } from './tasks.service';
import { TasksController } from './tasks.controller';
import { Task } from './task.entity';
import { Community } from '../communities/community.entity';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';
import { MailerService } from '../mailer/mailer.service';
import { Message } from '../messages/message.entity';
import { Session } from '../session/session.entity';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Task]),
    TypeOrmModule.forFeature([Community]),
    TypeOrmModule.forFeature([User]),
    TypeOrmModule.forFeature([Message]),
    TypeOrmModule.forFeature([Session]),
    TypeOrmModule.forFeature([CommunityUserStatus]),
  ],
  controllers: [TasksController],
  providers: [
    TasksService,
    UsersService,
    MailerService,
    ...communitiesProviders,
    ...usersProviders,
    ...tasksProviders,
  ],
  exports: [TasksService],
})
export class TasksModule {}
