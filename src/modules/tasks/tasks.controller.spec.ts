import { Test, TestingModule } from '@nestjs/testing';
import { TasksController } from './tasks.controller';
import { TasksService } from './tasks.service';
import { CommunitiesService } from '../communities/communities.service';
import { UsersService } from '../users/users.service';
import { SessionsService } from '../session/sessions.service';
import { MessagesService } from '../messages/messages.service';
import { createConnection } from 'typeorm';
import { Community } from '../communities/community.entity';
import { Task } from './task.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../users/user.entity';
import { Session } from '../session/session.entity';
import { Message } from '../messages/message.entity';
import { CommunityUserStatus } from '../communityUserStatus/communityUserStatus.entity';

import {
  newUser,
  newTask,
  newCommunity,
  getSession,
  existingCurator,
} from '../../common/helpers/test/fixtures';
import { Statuses } from '../users/statuses.enum';
import { S3uploaderService } from '../s3uploader/s3uploader.service';
import { MailerService } from '../mailer/mailer.service';
import httpMocks from 'node-mocks-http';
import { TaskStatuses } from './taskStatuses.enum';

const request = httpMocks.createRequest({
  method: 'PUT',
  url: '/communities/:id',
});
const session = getSession(request, existingCurator.user);

describe('TaskController', () => {
  let tasksModule: TestingModule;
  let tasksController: TasksController;
  let tasksService: TasksService;
  let communitiesService: CommunitiesService;
  let usersService: UsersService;
  let s3uploaderService: S3uploaderService;

  beforeAll(async () => {
    const testConnection = await createConnection({
      type: 'sqljs',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      logging: false,
      dropSchema: true,
      synchronize: true,
    });
    const communitiesRepository = testConnection.getRepository(Community);
    const tasksRepository = testConnection.getRepository(Task);
    const usersRepository = testConnection.getRepository(User);
    const sessionsRepository = testConnection.getRepository(Session);
    const messagesRepository = testConnection.getRepository(Message);
    const communityUserStatusRepository = testConnection.getRepository(
      CommunityUserStatus,
    );
    tasksModule = await Test.createTestingModule({
      controllers: [TasksController],
      providers: [
        {
          provide: getRepositoryToken(CommunityUserStatus),
          useValue: communityUserStatusRepository,
        },
        MailerService,
        TasksService,
        {
          provide: getRepositoryToken(Task),
          useValue: tasksRepository,
        },
        CommunitiesService,
        {
          provide: getRepositoryToken(Community),
          useValue: communitiesRepository,
        },
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: usersRepository,
        },
        SessionsService,
        {
          provide: getRepositoryToken(Session),
          useValue: sessionsRepository,
        },
        MessagesService,
        {
          provide: getRepositoryToken(Message),
          useValue: messagesRepository,
        },
        S3uploaderService,
      ],
    }).compile();

    tasksController = tasksModule.get<TasksController>(TasksController);
    tasksService = tasksModule.get<TasksService>(TasksService);
    communitiesService = tasksModule.get<CommunitiesService>(
      CommunitiesService,
    );
    usersService = tasksModule.get<UsersService>(UsersService);
    s3uploaderService = tasksModule.get<S3uploaderService>(S3uploaderService);
  });

  describe('tasks', () => {
    let thisCommunity;
    let user;
    let taskCurator;

    beforeAll(async () => {
      thisCommunity = await communitiesService.createAndSave(newCommunity);
      await usersService.createAndSave(newUser);
      user = await usersService.findOneByAddress(newUser.address);
      await usersService.createAndSave(existingCurator.user);
      taskCurator = await usersService.findOne(existingCurator.user.id);
      taskCurator.curatorOf.push(thisCommunity);
      await usersService.save(taskCurator);
    });

    it('should create and save a task', async () => {
      const result = await tasksController.create(newTask, session);
      expect(result.title).toBe(newTask.title);
      expect(typeof result.contractId).toBe('string');
    });

    it('should be associated with a community', async () => {
      const result = await tasksController.create(newTask, session);
      const thisTask = await tasksController.findOne(result.id);
      expect(thisTask.community.id).toBe(thisCommunity.id);
    });

    it('should claim a task', async () => {
      const result = await tasksController.create(newTask, session);
      const email = 'foss@setFlagsFromString.com';
      user.memberOf.push(result.community);
      await usersService.save(user);
      await tasksController.claim(
        {
          taskId: result.id,
          email,
        },
        {
          user,
        },
      );
      const thisTask = await tasksController.findOne(result.id);
      expect(thisTask.status).toBe('claimed');
      expect(thisTask.claimedBy).toBe(user.id);
      expect(thisTask.userEmail).toBe(email);
    });

    it('should claim a task and find all by community', async () => {
      const result = await tasksController.create(newTask, session);
      const email = 'foss@setFlagsFromString.com';
      user.memberOf.push(result.community);
      await usersService.save(user);
      await tasksController.claim(
        {
          taskId: result.id,
          email,
        },
        {
          user,
        },
      );
      const tasks = await tasksService.findAllByCommunity(newTask.communityId);
      const thisTask = tasks.find((task) => task.id === result.id);
      expect(thisTask.claimedBy).toBe(user.id);
    });

    it('should throw an exception if user is pending', async () => {
      const result = await tasksController.create(newTask, session);
      const email = 'foss@setFlagsFromString.com';
      user.memberOf.push(result.community);
      user.status = Statuses.Pending;
      try {
        await tasksController.claim(
          {
            taskId: result.id,
            email,
          },
          {
            user,
          },
        );
      } catch (error) {
        expect(error.message).toBe(
          'You cannot perform this action while a transaction is pending.',
        );
      }
    });

    it('should submit a task', async () => {
      const result = await tasksController.create(newTask, session);
      const email = 'foss@setFlagsFromString.com';
      user.memberOf.push(result.community);
      user.status = null;
      await tasksController.submit(
        {
          taskId: result.id,
          email,
        },
        {
          user,
        },
      );
      const thisTask = await tasksController.findOne(result.id);
      expect(thisTask.status).toBe(TaskStatuses.PendingApproval);
    });

    it('should confirm a task', async () => {
      const result = await tasksController.create(newTask, session);
      await tasksService.approveSubmission({
        taskId: result.id,
      });
      const thisTask = await tasksController.findOne(result.id);
      expect(thisTask.status).toBe('approved');
    });

    it('should complete a task', async () => {
      const result = await tasksController.create(newTask, session);
      const task = await tasksController.findOne(result.id);
      task.startDate = task.startDate.toISOString();
      task.endDate = task.endDate.toISOString();
      await tasksService.complete(task.contractId);
      const thisTask = await tasksController.findOne(result.id);
      expect(thisTask.status).toBe('completed');
    });

    it('should cancel a task', async () => {
      const result = await tasksController.create(newTask, session);
      const task = await tasksController.findOne(result.id);
      task.startDate = task.startDate.toISOString();
      task.endDate = task.endDate.toISOString();
      await tasksService.cancel(task.contractId);
      const thisTask = await tasksController.findOne(result.id);
      expect(thisTask.status).toBe('canceled');
    });

    it('should set a task to pendingCancellation', async () => {
      await usersService.createAndSave(existingCurator.user);
      const result = await tasksController.create(newTask, session);
      const task = await tasksController.findOne(result.id);
      await tasksController.setPendingCancellation(
        { taskId: task.id },
        session,
      );
      const thisTask = await tasksController.findOne(result.id);
      expect(thisTask.status).toBe(TaskStatuses.PendingCancellation);
    });

    it('should set a deny a claim', async () => {
      await usersService.createAndSave(existingCurator.user);
      const result = await tasksController.create(newTask, session);
      const task = await tasksController.findOne(result.id);
      await tasksController.denyClaim({ taskId: task.id }, session);
      const thisTask = await tasksController.findOne(result.id);
      expect(thisTask.claimedBy).toBe(null);
      expect(thisTask.status).toBe(TaskStatuses.Escrowed);
    });
  });
});
