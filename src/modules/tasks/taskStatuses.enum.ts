export enum TaskStatuses {
  Initialized = 'initialized',
  Escrowed = 'escrowed',
  Claimed = 'claimed',
  PendingApproval = 'pendingApproval',
  Approved = 'approved',
  Completed = 'completed',
  PendingCancellation = 'pendingCancellation',
  Canceled = 'canceled',
}
