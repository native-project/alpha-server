import {
  Entity,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Community } from '../communities/community.entity';
import {
  Length,
  IsInt,
  Min,
  IsDateString,
  Validate,
  IsOptional,
  IsEmail,
} from 'class-validator';
import { User } from '../users/user.entity';
import { ChoiceName } from '../../common/helpers/utils/choiceValidator';
import { DateStringCompare } from './dateStringCompareValidator';
import { TimeUnits } from './timeUnits.enum';
import { TaskStatuses } from './taskStatuses.enum';

@Entity()
export class Task {
  @PrimaryGeneratedColumn('uuid') id: string;

  @Column({ type: 'text', nullable: true })
  contractId: string;

  @ManyToOne((type) => Community, (community) => community.tasks, {
    eager: true,
  })
  community: Community;

  @Column({ readonly: true })
  @Length(2, 128)
  title: string;

  @Column() reward: number;

  @Column('text') description: string;

  @Column()
  @IsDateString()
  startDate: Date;

  @Column()
  @IsDateString()
  @Validate(DateStringCompare, ['after', 'startDate'])
  endDate: Date;

  @Column({ nullable: true })
  @IsDateString()
  @IsOptional()
  claimedDate: Date;

  @Column({ nullable: true })
  @IsDateString()
  @IsOptional()
  completedDate: Date;

  @Column({ nullable: true })
  @IsDateString()
  @IsOptional()
  canceledDate: Date;

  @Column({ type: 'varchar', default: TimeUnits.Minutes })
  @Validate(ChoiceName, [TimeUnits])
  @IsOptional()
  timeToCompleteUnit: TimeUnits;

  @Column()
  @IsInt()
  @Min(0)
  timeToComplete: number;

  @Column({ nullable: true })
  @IsOptional()
  imageUrl: string;

  @Column({ type: 'varchar', default: TaskStatuses.Initialized })
  @Validate(ChoiceName, [TaskStatuses])
  @IsOptional()
  status: TaskStatuses;

  @ManyToOne((type) => User, (user) => user.tasks)
  claimedBy: User;

  @Column({ nullable: true })
  @IsEmail()
  @IsOptional()
  userEmail: string;

  @Column({ type: 'text', nullable: true })
  comments: string;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;
}
