import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { TaskStatuses } from './taskStatuses.enum';
import { Community } from '../communities/community.entity';
import { User } from '../users/user.entity';
import { TaskDto } from './task.dto';
import { validate } from 'class-validator';
import { ClaimRequest, ConfirmRequest, SubmitRequest } from './request.dto';
import { MailerService } from '../mailer/mailer.service';
import { generateContractId } from '../../common/helpers/utils/utils';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task) private readonly tasksRepository: Repository<Task>,
    @InjectRepository(Community)
    private readonly communitiesRepository: Repository<Community>,
  ) {}

  mailerService = new MailerService();

  async findOne(id): Promise<Task> {
    const task = await this.tasksRepository.findOne({
      where: {
        id,
      },
      relations: ['claimedBy'],
    });
    return this.claimedByMapper(task);
  }

  claimedByMapper(element) {
    const newElement = element;
    if (element.claimedBy && element.claimedBy.id) {
      newElement.claimedBy = element.claimedBy.id;
    }
    return newElement;
  }

  async findAllByCommunity(communityId): Promise<Task[]> {
    const community: Community = await this.communitiesRepository.findOne(
      communityId,
    );

    let tasks = await this.tasksRepository.find({
      where: {
        community,
      },
      relations: ['claimedBy'],
    });
    tasks = tasks.map(this.claimedByMapper);
    return tasks;
  }

  async createAndSave(taskDto: TaskDto): Promise<Task> {
    const task: Task = this.create(taskDto);
    task.status = TaskStatuses.Initialized;

    const errors = await validate(task);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }

    const community: Community = await this.communitiesRepository.findOne(
      taskDto.communityId,
    );
    if (community === undefined) {
      throw new HttpException('Invalid community', HttpStatus.BAD_REQUEST);
    }
    task.community = community;

    task.status = TaskStatuses.Initialized;
    task.contractId = await generateContractId(task, this.tasksRepository);

    return await this.tasksRepository.save(task);
  }

  async escrow(contractId: number): Promise<Task> {
    const task: Task = await this.tasksRepository.findOne({
      where: {
        contractId,
      },
    });

    task.status = TaskStatuses.Escrowed;
    await this.mailerService.sendTaskEscrowedEmail(
      task.community.curatorEmail,
      task,
    );
    return await this.tasksRepository.save(task);
  }

  async claim(taskRequest: ClaimRequest, user: User): Promise<Task> {
    const errors = await validate(taskRequest);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const task: Task = await this.tasksRepository.findOne({
      id: taskRequest.taskId,
    });

    task.claimedBy = user;
    task.status = TaskStatuses.Claimed;
    task.userEmail = taskRequest.email;
    task.claimedDate = new Date();
    await this.mailerService.sendTaskClaimedEmail(task);
    const savedTask = await this.tasksRepository.save(task);
    return this.claimedByMapper(savedTask);
  }

  async submitForApproval(submitRequest: SubmitRequest): Promise<Task> {
    const errors = await validate(submitRequest);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const task: Task = await this.tasksRepository.findOne({
      id: submitRequest.taskId,
    });
    task.status = TaskStatuses.PendingApproval;
    return await this.tasksRepository.save(task);
  }

  async approveSubmission(taskRequest: ConfirmRequest): Promise<Task> {
    const errors = await validate(taskRequest);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const task: Task = await this.tasksRepository.findOne({
      id: taskRequest.taskId,
    });
    task.status = TaskStatuses.Approved;
    return await this.tasksRepository.save(task);
  }

  async denyClaim(task: Task): Promise<Task> {
    task.claimedBy = null;
    task.claimedDate = null;
    return await this.updateStatus(task, TaskStatuses.Escrowed);
  }

  async denySubmission(task: Task): Promise<Task> {
    this.mailerService.sendTaskSubmissionDenied(
      task.community.curatorEmail,
      task,
    );
    this.mailerService.sendTaskSubmissionDenied(task.userEmail, task);
    return await this.updateStatus(task, TaskStatuses.Claimed);
  }

  async complete(taskContractId): Promise<Task> {
    const task: Task = await this.tasksRepository.findOne({
      contractId: taskContractId,
    });
    task.status = TaskStatuses.Completed;
    task.completedDate = new Date();
    this.mailerService.sendTaskCompleteEmail(task.community.curatorEmail, task);
    this.mailerService.sendTaskCompleteEmail(task.userEmail, task);
    return await this.tasksRepository.save(task);
  }

  async cancel(taskContractId): Promise<Task> {
    const task: Task = await this.tasksRepository.findOne({
      contractId: taskContractId,
    });
    task.status = TaskStatuses.Canceled;
    task.canceledDate = new Date();
    this.mailerService.sendTaskCanceledEmail(task.community.curatorEmail, task);
    this.mailerService.sendTaskCanceledEmail(task.userEmail, task);
    return await this.tasksRepository.save(task);
  }

  async updateStatus(task: Task, status: TaskStatuses): Promise<Task> {
    task.status = status;
    return await this.tasksRepository.save(task);
  }

  create(tasksDto: TaskDto): Task {
    return this.tasksRepository.create(tasksDto);
  }
}
