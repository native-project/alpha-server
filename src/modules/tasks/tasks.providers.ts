import { Connection } from 'typeorm';
import { Task } from './task.entity';

export const tasksProviders = [
  {
    provide: 'TasksRepository',
    useFactory: (connection: Connection) => connection.getRepository(Task),
    inject: [Connection],
  },
];
