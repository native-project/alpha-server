export enum TimeUnits {
  Minutes = 'minutes',
  Hours = 'hours',
  Days = 'days',
}
