import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

import moment from 'moment';

@ValidatorConstraint({ name: 'dateStringCompare', async: false })
export class DateStringCompare implements ValidatorConstraintInterface {
  /*
    Date string validation to compare that one date is greater than 
      another in the model
    args[0]: string - ('before' or 'after' to set compare operator)
    args[1]: string - (date field to compare against)
    */
  validate(dateString: string, args: ValidationArguments) {
    if (args.constraints[0] === 'after') {
      return moment(dateString).isAfter(args.object[args.constraints[1]]);
    } else if (args.constraints[0] === 'before') {
      return moment(dateString).isBefore(args.object[args.constraints[1]]);
    } else {
      return false;
    }
  }

  defaultMessage(args: ValidationArguments) {
    return `The unit name must be blah`;
  }
}
