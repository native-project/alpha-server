import { User } from '../users/user.entity';
import { TimeUnits } from './timeUnits.enum';
import { TaskStatuses } from './taskStatuses.enum';

export class TaskDto {
  communityId: number;
  title: string;
  reward: number;
  description: string;
  startDate: string;
  endDate: string;
  claimedDate: string;
  completedDate: string;
  timeToCompleteUnit: TimeUnits;
  timeToComplete: number;
  imageUrl: string;
  status: TaskStatuses;
  claimedBy: User;
  comments: string;
}
