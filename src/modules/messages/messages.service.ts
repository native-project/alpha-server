import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Message } from './message.entity';
import { MessageDto } from './message.dto';
import { validate } from 'class-validator';

@Injectable()
export class MessagesService {
  constructor(
    @InjectRepository(Message)
    private readonly messagesRepository: Repository<Message>,
  ) {}

  async findOneByText(text): Promise<Message> {
    return await this.messagesRepository.findOne({
      where: {
        text,
      },
    });
  }

  async createAndSave(messageDto: MessageDto): Promise<Message> {
    const errors = await validate(messageDto);
    if (errors.length > 0) {
      throw new HttpException(
        {
          status: HttpStatus.BAD_REQUEST,
          error: `${errors}`,
        },
        400,
      );
    }
    const message: Message = this.create(messageDto);
    return await this.messagesRepository.save(message);
  }

  create(messageDto: MessageDto): Message {
    return this.messagesRepository.create(messageDto);
  }
}
