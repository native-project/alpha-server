import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { messagesProviders } from './messages.providers';
import { Message } from './message.entity';
import { MessagesService } from './messages.service';

@Module({
  imports: [TypeOrmModule.forFeature([Message])],
  providers: [MessagesService, ...messagesProviders],
  exports: [MessagesService],
})
export class MessagesModule {}
