export enum Messages {
  Staked = 'You have successfully staked into the community.',
  Unstaked = 'You have successfully unstaked from the community.',
  PreStake = 'Your transaction to stake into the community is pending confirmation.',
}
