import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { User } from '../users/user.entity';
import { Messages } from './messages.enum';

@Entity()
export class Message {
  @PrimaryGeneratedColumn() id: number;

  @Column({ type: 'varchar' })
  text: Messages;

  @ManyToMany((type) => User, (user) => user.messages)
  @JoinTable({ name: 'user_messages' })
  user: User;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;
}
