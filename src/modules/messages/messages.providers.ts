import { Connection } from 'typeorm';
import { Message } from './message.entity';

export const messagesProviders = [
  {
    provide: 'MessagesRepository',
    useFactory: (connection: Connection) => connection.getRepository(Message),
    inject: [Connection],
  },
];
