import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { BlockchainModule } from './modules/blockchain/blockchain.module';
import { BlockchainScannerService } from './modules/blockchain/blockchainscanner.service';
import { CommunityContractsService } from './modules/blockchain/communitycontracts.service';
import { Cron } from 'vineyard-cron';
import { lastBlockTypes } from './modules/blockchain/lastBlockTypes.enum';

class Scanner {
  name: string;
  type: lastBlockTypes;
}

(async () => {
  const app = await bootstrapApp();
  const blockchainScannerService = await getBlockchainScannerService(app);
  const communityContractService = await getCommunityContractsService(app);
  try {
    startCron(
      blockchainScannerService,
      communityContractService,
      [
        { name: 'Blockchain Head Scanner', type: lastBlockTypes.Head },
        {
          name: 'Blockchain Confirmation Scanner',
          type: lastBlockTypes.Confirmation,
        },
      ],
      3000,
    );
  } catch (error) {
    console.log(error); // tslint:disable-line
  }
})();

export async function startCron(
  blockchainService,
  communityContractsService,
  scanTypes: Scanner[],
  timeout: number,
) {
  const scanTypeArray = [];
  // for each type of scan push an object describing it
  scanTypes.map((scanType) => {
    scanTypeArray.push({
      name: scanType.name,
      action: () =>
        blockchainService.scan(communityContractsService, scanType.type),
    });
  });

  const blockchainScannerCron = new Cron(scanTypeArray, timeout);
  return await blockchainScannerCron.start();
}

async function bootstrapApp() {
  return await NestFactory.createApplicationContext(AppModule);
}

async function getBlockchainScannerService(app) {
  const module = await app.select(BlockchainModule);
  return await module.get(BlockchainScannerService);
}

async function getCommunityContractsService(app) {
  const module = await app.select(BlockchainModule);
  return await module.get(CommunityContractsService);
}
