import { Connection } from 'typeorm';
import { Session } from './modules/session/session.entity';

export const appProviders = [
  {
    provide: 'SessionRepository',
    useFactory: (connection: Connection) => connection.getRepository(Session),
    inject: [Connection],
  },
];
