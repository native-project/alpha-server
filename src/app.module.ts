import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from './config.module';
import { UsersModule } from './modules/users/users.module';
import { UsersController } from './modules/users/users.controller';
import { CommunitiesController } from './modules/communities/communities.controller';
import { CommunitiesModule } from './modules/communities/communities.module';
import { TasksModule } from './modules/tasks/tasks.module';
import { TasksController } from './modules/tasks/tasks.controller';
import { ProjectsModule } from './modules/projects/projects.module';
import { ProjectsController } from './modules/projects/projects.controller';
import { PollsModule } from './modules/polls/polls.module';
import { PollsController } from './modules/polls/polls.controller';
import { VotesModule } from './modules/votes/votes.module';
import { MessagesModule } from './modules/messages/messages.module';
import { SessionMiddleware } from './modules/session/session.middleware';
import { appProviders } from './app.providers';
import { BlockchainModule } from './modules/blockchain/blockchain.module';
import { S3uploaderModule } from './modules/s3uploader/s3uploader.module';
import { MailerModule } from './modules/mailer/mailer.module';
import { TypeOrmModuleOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';
import { ConnectionOptions } from 'typeorm';
import { ormConfig } from './ormconfig';

const castConfig = ormConfig as TypeOrmModuleOptions &
  Partial<ConnectionOptions>;

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRoot(castConfig),
    UsersModule,
    CommunitiesModule,
    TasksModule,
    ProjectsModule,
    PollsModule,
    VotesModule,
    MessagesModule,
    BlockchainModule,
    S3uploaderModule,
    MailerModule,
  ],
  controllers: [
    UsersController,
    CommunitiesController,
    TasksController,
    ProjectsController,
    PollsController,
  ],
  providers: [...appProviders],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
    consumer
      .apply(SessionMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
