import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: `${process.env.CLIENT_URL}${
      process.env.CLIENT_PORT ? ':' + process.env.CLIENT_PORT : ''
    }`,
    credentials: true,
  });
  await app.listen(process.env.API_PORT);
}

bootstrap();
