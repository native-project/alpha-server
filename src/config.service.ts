const dotenv = require('dotenv'); /* tslint:disable-line */

export class ConfigService {
  constructor(filePath: string) {
    dotenv.config();
  }
}
