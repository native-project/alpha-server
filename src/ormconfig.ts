import ormConfigJson from '../ormconfig.json';
if (ormConfigJson.entities.length > 0) {
  const entities = ormConfigJson.entities[0];
  if (process.env.NODE_ENV === 'production') {
    ormConfigJson.entities[0] = entities.replace('src/', 'dist/');
    delete ormConfigJson.migrations;
    delete ormConfigJson.cli.migrationsDir;
  } else {
  }
}
export const ormConfig = ormConfigJson;
