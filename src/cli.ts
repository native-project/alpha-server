import { NestFactory } from '@nestjs/core';
import { BlockchainModule } from './modules/blockchain/blockchain.module';
import { CommunityContractsService } from './modules/blockchain/communitycontracts.service';
import { CommunitiesModule } from './modules/communities/communities.module';
import { CommunitiesService } from './modules/communities/communities.service';
import { UsersModule } from './modules/users/users.module';
import { UsersService } from './modules/users/users.service';
import { AppModule } from './app.module';
import ContractCli from './common/cli/ContractCli';
import UsersCli from './common/cli/UsersCli';

import { prompt } from 'inquirer';

(async () => {
  const getCommand = await prompt([
    {
      type: 'list',
      name: 'command',
      message: 'What would you like to do?',
      choices: getMergedChoices(),
      filter: (val) => {
        return val.toLowerCase();
      },
    },
  ]);

  if (getCommand.command) {
    const command = getCommand.command;
    const app = await bootstrapApp();
    const domain = command.split(':')[0];
    let cli;

    if (!command) {
      process.exit();
    }

    switch (domain) {
      case ContractCli.NAME.toLowerCase():
        const communityContractsService = await getCommunityContractsService(
          app,
        );
        const communitiesService = await getCommunitiesService(app);
        cli = new ContractCli(communityContractsService, communitiesService);
        await cli.switch(command);
        break;
      case UsersCli.NAME.toLowerCase():
        const usersService = await getUsersService(app);
        const communitiesService2 = await getCommunitiesService(app);
        cli = new UsersCli(usersService, communitiesService2);
        await cli.switch(command);
        break;
      default:
        process.exit();
    }
  } else {
    process.exit();
  }
  process.exit();
})();

function getMergedChoices() {
  const choices = [];
  const contract = ContractCli.getChoices();
  const users = UsersCli.getChoices();
  return choices.concat(contract, users);
}
async function bootstrapApp() {
  return await NestFactory.createApplicationContext(AppModule);
}

async function getCommunityContractsService(app) {
  const module = await app.select(BlockchainModule);
  return await module.get(CommunityContractsService);
}

async function getCommunitiesService(app) {
  const module = await app.select(CommunitiesModule);
  return await module.get(CommunitiesService);
}

async function getUsersService(app) {
  const module = await app.select(UsersModule);
  return await module.get(UsersService);
}
