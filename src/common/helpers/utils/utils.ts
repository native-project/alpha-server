import seedrandom from 'seedrandom';
import BigNumber from 'bignumber.js';

export const isUrl = (test) => {
  const re = new RegExp(
    '[(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)',
  );
  return re.test(test);
};

export const lossyHashToInt = (valToBeHashed) => {
  let hash = '';
  const solidityMaxInt = new BigNumber(2 ** 255);

  valToBeHashed = valToBeHashed.toString().split('');

  for (const char of valToBeHashed) {
    const seededRng = seedrandom(char.charCodeAt(0).toString());
    const rngVal = (seededRng() * 10).toFixed(0).toString();
    const possibleNewHash = new BigNumber(hash + rngVal);
    if (possibleNewHash.lt(solidityMaxInt)) {
      hash = hash + rngVal;
    }
  }
  return hash;
};

export const generateContractId = async (
  entity,
  repository,
): Promise<string> => {
  let contractId;
  for (let i = 1; i < i + 1; i++) {
    contractId = lossyHashToInt(entity.id + i.toString());
    const foundEntity = await repository.findOne({
      contractId,
    });

    if (foundEntity === undefined) {
      return contractId;
    }
  }
  return contractId;
};
