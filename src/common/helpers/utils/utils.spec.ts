import { lossyHashToInt } from './utils';

describe('utils', () => {
  beforeAll(async () => {});

  describe('lossyHashToInt', () => {
    it('should hash a string into an int', async () => {
      const hash = lossyHashToInt('abc');
      expect(hash).toBe('1011');
    });

    it('should hash an int into an int', async () => {
      const hash = lossyHashToInt('123');
      expect(hash).toBe('627');
    });

    it('should hash an int into an int', async () => {
      const hash = lossyHashToInt(123);
      expect(hash).toBe('627');
    });
    it('should hash a uuid into an int', async () => {
      const hash = lossyHashToInt('A0EEBC99-9C0B-4EF8-BB6D-6BB9BD380A11');
      expect(hash).toBe('109557755757977551027771871775787291066');
    });
  });
});
