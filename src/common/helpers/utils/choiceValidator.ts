import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'choiceName', async: false })
export class ChoiceName implements ValidatorConstraintInterface {
  validate(name: string, args: ValidationArguments) {
    return (Object as any).values(args.constraints[0]).includes(name);
  }

  defaultMessage(args: ValidationArguments) {
    return `The unit name must be ${Object.keys(args.constraints[0]).join(
      ', ',
    )}`;
  }
}
