import Cookie from 'express-session/session/cookie';
import { SocialLinks } from '../../../modules/communities/sociallinks.enum';

export const newCommunity = {
  name: 'Community Name',
  address: '0x9BD0ae08A7C24af62b2581f878C3B08E0619227A',
  tokenAddress: '0xsomethingelse',
  loggerAddress: '0xsomethingelse',
  quorum: 50,
  support: 50,
  memberCount: 0,
  image: 'assets/communities/community_header.png',
  icon: 'assets/communities/community_icon.png',
  location: 'Test, Location',
  subtitle: 'Test subtitle',
  dataImage: 'assets/line-01.png',
  curatorEmail: 'test@ibn.com',
  communityIntro: 'Lorem Ipsum',
  communityPurpose: 'Lorem Upsum',
  membershipBenefits: JSON.stringify([
    'Lorem ipsum dolor sit amet',
    'Test another benefit',
    'Third benefit',
  ]),
  votingPolicy: 'assets/communities/voting_policy.pdf',
  revenueDistributionPolicy:
    'assets/communities/revenue_distribution_policy.pdf',
  socialMediaLinks: JSON.stringify([
    { name: SocialLinks.Facebook, link: 'https://facebook.com/test' },
    { name: SocialLinks.Twitter, link: 'https://twitter.com/test' },
    { name: SocialLinks.Instagram, link: 'https://instagram.com/test' },
  ]),
  curatorInfo: 'Michael Conway',
  telegramLink: 'https://telegram.com/test',
  isPrivate: false,
};

export const newUser = {
  address: '0x08b59c2ea41798e221f0884f44a742c73069c483',
};

export const newUser2 = {
  address: '0x09c59c2ea41798e221f0884f44a742c73069c483',
};

export const newUser3 = {
  address: '0x07d59c2ea41798e221f0884f44a742c73069c483',
};

export const newUser4 = {
  address: '0x0bd59c2ea41798e221f0884f44a742c73069bgt',
};

export const newUser5 = {
  address: '0x0dd59c2eb41798e2Y1f0d84f44a742c73069c345',
};

export const newUser6 = {
  address: '0x16d59c2eb41798e2Y1f0d84f44a742c7306n3n45',
};

export const newSession = {
  expiredAt: 1536359650737,
  id: 'zKjG3gUPqfQrqA7MGF53A0WlBp5zdK8b',
  json:
    '{"cookie":{"originalMaxAge":null,"expires":null,"httpOnly":true,"path":"/"},"user":{"id":1,"address":"0x08b59c2ea41798e221f0884f44a742c73069c483","signing":null,"role":null,"createdAt":"2018-09-06T16:11:17.744Z","updatedAt":"2018-09-06T22:34:10.726Z","memberOf":[{"id":2,"name":"Imaginal Films","address":"0x6624260eE92a79660DF689F6e51C6EFa0B1c067c","tokenAddress":"0x71b455935d78666dc2eac04b3b961c5a13ea0800","loggerAddress":"0x6624260eE92a79660DF689F6e51C6EFa0B1c067e","image":"static/media/imaginal_header.png","icon":"static/media/imaginal_icon.png","location":"Boulder, Colorado","subtitle":"Film","dataImage":"static/media/line-03.png","communityIntro":"Creating a new blueprint for the future of humanity","communityPurpose":"To inspire the world to greatness by bringing filmmakers and audiences together in a film funding, sharing and viewing collective.","quorum":5,"createdAt":"2018-09-06T16:13:29.430Z","updatedAt":"2018-09-06T16:13:29.430Z"}]}}', // tslint:disable-line
};

export const newTask = {
  title: 'Test Task 1',
  communityId: 1,
  reward: 500000,
  description: 'Task Description',
  startDate: '2018-08-01T06:00:00.000Z',
  endDate: '2018-09-01T06:00:00.000Z',
  claimedDate: null,
  completedDate: null,
  timeToCompleteUnit: 'hours',
  timeToComplete: 24,
  imageUrl: 'http://imgurl.ca',
  status: 'created',
  claimedBy: null,
  comments: 'comments',
};

export const newProject = {
  title: 'Test Project 1',
  communityId: 1,
  subtitle: 'Project Subtitle',
  description: 'Description here.',
  additionalInfo: 'additional info',
  totalCost: 5000,
  startDate: '2018-08-01T06:00:00.000Z',
  endDate: '2019-08-01T06:00:00.000Z',
  costBreakdownUrl: 'http://costbreakdown.de',
  roadmapUrl: 'http://roadmap.de',
  imageUrl: 'http://prjectaddress.de/img',
  address: 'http://projectaddress.de',
  status: 'initialized',
};

export const newPoll = {
  title: 'Test Poll 1',
  communityId: 1,
  question: 'Project Question',
  description: 'Description here.',
  fileUrl: 'http://prjectaddress.de/file',
  startDate: '2018-08-01T06:00:00.000Z',
  endDate: '2019-08-01T06:00:00.000Z',
  options: [],
};

export const newOption1 = {
  poll: null,
  name: 'yes',
};

export const newOption2 = {
  poll: null,
  name: 'no',
};

export const newOption3 = {
  poll: null,
  name: 'maybe',
};

export const existingUser = {
  user: {
    id: 2,
    address: '0xdFf58D2bC86a8480D5B508c677dB28188351d50A',
    alias: null,
    country: null,
    state: null,
    city: null,
    email: null,
    telegram: null,
    preferredContact: 'email',
    signing: null,
    role: null,
    createdAt: '2018-08-10T21:41:53.231Z',
    updatedAt: '2018-08-13T03:24:26.296Z',
  },
};

export const existingCurator = {
  user: {
    id: 2,
    address: '0xdFf58D2bC86a8480D5B508c677dB28188351d50A',
    alias: null,
    country: null,
    state: null,
    city: null,
    email: null,
    telegram: null,
    preferredContact: 'email',
    signing: null,
    role: 'curator',
    createdAt: '2018-08-10T21:41:53.231Z',
    updatedAt: '2018-08-13T03:24:26.296Z',
  },
};

export const getSession = (request, user = existingUser.user) => {
  return {
    cookie: new Cookie(),
    req: request,
    id: 'WhY8rZ0rAsibnpCaaxUNjdNUFoHHkEnJ',
    reload: () => {},
    save: () => {},
    user,
    regenerate: () => {},
    destroy: () => {},
    touch: () => {},
  };
};
