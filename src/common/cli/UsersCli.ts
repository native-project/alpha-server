import { UsersService } from '../../modules/users/users.service';
import { CommunitiesService } from '../../modules/communities/communities.service';
import { ICli } from './ICli';
import { prompt } from 'inquirer';

export default class UsersCli implements ICli {
  public static NAME = 'Users';
  public static STAKE_MEMBER = 'Users: Stake Member';
  public static ADD_CURATOR = 'Users: Add Curator';
  private service: UsersService;
  private communitiesService: CommunitiesService;

  constructor(service: UsersService, communitiesService: CommunitiesService) {
    this.service = service;
    this.communitiesService = communitiesService;
  }
  async switch(command: string) {
    let response;
    switch (command) {
      case UsersCli.STAKE_MEMBER.toLowerCase():
        const userInput = await prompt([
          {
            type: 'input',
            name: 'user',
            message: 'What is ethereum address of the user to stake?',
          },
        ]);
        const communityInput = await prompt([
          {
            type: 'input',
            name: 'community',
            message: 'What is community id to stake into?',
          },
        ]);
        if (
          userInput &&
          userInput.user &&
          communityInput &&
          communityInput.community
        ) {
          const community = await this.communitiesService.findOne(
            communityInput.community,
          );
          if (community) {
            response = await this.service.stake(userInput.user, community);
          }
        }

        if (response) {
          console.log('Successfully staked member.'); // tslint:disable-line
        } else {
          console.log('Staking unsuccessful.'); // tslint:disable-line
        }
        break;
      case UsersCli.ADD_CURATOR.toLowerCase():
        const curatorInput = await prompt([
          {
            type: 'input',
            name: 'user',
            message: 'What is ethereum address of the user to add as curator?',
          },
        ]);
        const communityInputCurator = await prompt([
          {
            type: 'input',
            name: 'community',
            message: 'What is community id to add the curator to?',
          },
        ]);
        if (
          curatorInput &&
          curatorInput.user &&
          communityInputCurator &&
          communityInputCurator.community
        ) {
          const community = await this.communitiesService.findOne(
            communityInputCurator.community,
          );
          if (community) {
            response = await this.service.addCurator(
              curatorInput.user,
              community,
            );
          }
        }

        if (response) {
          console.log('Successfully added curator.'); // tslint:disable-line
        } else {
          console.log('Curator add unsuccessful.'); // tslint:disable-line
        }
        break;
      default:
        process.exit();
    }
    return response;
  }

  public static getChoices() {
    return [UsersCli.STAKE_MEMBER, UsersCli.ADD_CURATOR];
  }
}
