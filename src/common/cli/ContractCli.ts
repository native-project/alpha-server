import { CommunityContractsService } from '../../modules/blockchain/communitycontracts.service';
import { CommunitiesService } from '../../../src/modules/communities/communities.service';
import { ICli } from './ICli';
import { prompt } from 'inquirer';

export default class ContractCli implements ICli {
  public static NAME = 'Contracts';
  public static CONTRACTS_SYNC_ABIS = 'Contracts: Sync ABIs';
  public static CONTRACTS_GET_ACCOUNTS = 'Contracts: Get Accounts';
  public static CONTRACTS_IS_MEMBER = 'Contracts: Is Member?';
  private service: CommunityContractsService;
  private communitiesService: CommunitiesService;

  constructor(
    service: CommunityContractsService,
    communitiesService: CommunitiesService,
  ) {
    this.service = service;
    this.communitiesService = communitiesService;
  }
  async switch(command: string) {
    let response;
    switch (command) {
      case ContractCli.CONTRACTS_SYNC_ABIS.toLowerCase():
        response = await this.service.syncAbis();
        if (response) {
          console.log('Successfully synced contract ABIs.'); // tslint:disable-line
        }
        break;
      case ContractCli.CONTRACTS_GET_ACCOUNTS.toLowerCase():
        response = await this.service.getAccounts();
        console.log(response); // tslint:disable-line
        break;
      case ContractCli.CONTRACTS_IS_MEMBER.toLowerCase():
        const communities = await this.communitiesService.findAll();
        const communityChoices = [];
        for (const community of communities) {
          communityChoices.push({
            name: community.name,
            value: community.address,
          });
        }
        const communityInput = await prompt([
          {
            type: 'list',
            name: 'community',
            message: 'Which community would you like to check against?',
            choices: communityChoices,
          },
        ]);
        const communityContractAddress = communityInput.community;
        const addressInput = await prompt([
          {
            type: 'input',
            name: 'address',
            message: 'What is the address of the user to check?',
          },
        ]);
        if (addressInput && addressInput.address) {
          const userAddress = addressInput.address;
          response = await this.service.isCommunityMember(
            communityContractAddress,
            userAddress,
          );
          console.log(response); // tslint:disable-line
        }
        break;
      default:
        process.exit();
    }
    return response;
  }

  public static getChoices() {
    return [
      ContractCli.CONTRACTS_SYNC_ABIS,
      ContractCli.CONTRACTS_GET_ACCOUNTS,
      ContractCli.CONTRACTS_IS_MEMBER,
    ];
  }
}
