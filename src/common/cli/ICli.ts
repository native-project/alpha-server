export interface ICli {
  switch(command: string): Promise<any>;
}
