import { MigrationInterface, QueryRunner } from 'typeorm';

export class Support1545166730853 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "community" ADD "support" integer`);
    await queryRunner.query(`UPDATE "community" SET "support" = 50`);
    await queryRunner.query(
      `ALTER TABLE "community" ALTER COLUMN "support" SET NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "community" DROP COLUMN "support"`);
  }
}
