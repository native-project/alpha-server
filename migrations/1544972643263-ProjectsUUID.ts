import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectsUUID1544972643263 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "project" ADD "contractId" text`);
    await queryRunner.query(
      `ALTER TABLE "poll" DROP CONSTRAINT "FK_ef21f5baebde1281ea18a06ba4c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "project" DROP CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57"`,
    );
    await queryRunner.query(`ALTER TABLE "project" DROP COLUMN "id"`);
    await queryRunner.query(
      `ALTER TABLE "project" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`,
    );
    await queryRunner.query(
      `ALTER TABLE "project" ADD CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57" PRIMARY KEY ("id")`,
    );
    await queryRunner.query(`ALTER TABLE "poll" DROP COLUMN "projectId"`);
    await queryRunner.query(`ALTER TABLE "poll" ADD "projectId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "poll" ADD CONSTRAINT "FK_ef21f5baebde1281ea18a06ba4c" FOREIGN KEY ("projectId") REFERENCES "project"("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "poll" DROP CONSTRAINT "FK_ef21f5baebde1281ea18a06ba4c"`,
    );
    await queryRunner.query(`ALTER TABLE "poll" DROP COLUMN "projectId"`);
    await queryRunner.query(`ALTER TABLE "poll" ADD "projectId" integer`);
    await queryRunner.query(
      `ALTER TABLE "project" DROP CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57"`,
    );
    await queryRunner.query(`ALTER TABLE "project" DROP COLUMN "id"`);
    await queryRunner.query(`ALTER TABLE "project" ADD "id" SERIAL NOT NULL`);
    await queryRunner.query(
      `ALTER TABLE "project" ADD CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57" PRIMARY KEY ("id")`,
    );
    await queryRunner.query(
      `ALTER TABLE "poll" ADD CONSTRAINT "FK_ef21f5baebde1281ea18a06ba4c" FOREIGN KEY ("projectId") REFERENCES "project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(`ALTER TABLE "project" DROP COLUMN "contractId"`);
  }
}
