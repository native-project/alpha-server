import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserStatus1541738237452 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "community_user_status" ("userId" integer NOT NULL, "communityId" integer, "userStatus" character varying, CONSTRAINT "PK_b9116a7bd7ca0f20699b0333b7a" PRIMARY KEY ("userId"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "task" ALTER COLUMN "status" SET DEFAULT 'initialized'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "task" ALTER COLUMN "status" SET DEFAULT 'created'`,
    );
    await queryRunner.query(`DROP TABLE "community_user_status"`);
  }
}
