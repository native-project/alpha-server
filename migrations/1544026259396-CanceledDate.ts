import { MigrationInterface, QueryRunner } from 'typeorm';

export class CanceledDate1544026259396 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "task" ADD "canceledDate" TIMESTAMP`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "task" DROP COLUMN "canceledDate"`);
  }
}
