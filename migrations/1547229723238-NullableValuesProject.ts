import { MigrationInterface, QueryRunner } from 'typeorm';

export class NullableValuesProject1547229723238 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "project" ALTER COLUMN "costBreakdownUrl" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "project" ALTER COLUMN "roadmapUrl" DROP NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "project" ALTER COLUMN "roadmapUrl" SET NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "project" ALTER COLUMN "costBreakdownUrl" SET NOT NULL`,
    );
  }
}
