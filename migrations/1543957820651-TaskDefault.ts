import { MigrationInterface, QueryRunner } from 'typeorm';

export class TaskDefault1543957820651 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "task" ALTER COLUMN "imageUrl" DROP DEFAULT`,
    );
    await queryRunner.query(
      `ALTER TABLE "task" ALTER COLUMN "userEmail" DROP DEFAULT`,
    );
    await queryRunner.query(
      `ALTER TABLE "task" ALTER COLUMN "comments" DROP DEFAULT`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "task" ALTER COLUMN "comments" SET DEFAULT 'null'`,
    );
    await queryRunner.query(
      `ALTER TABLE "task" ALTER COLUMN "userEmail" SET DEFAULT 'null'`,
    );
    await queryRunner.query(
      `ALTER TABLE "task" ALTER COLUMN "imageUrl" SET DEFAULT 'null'`,
    );
  }
}
