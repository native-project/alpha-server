import { MigrationInterface, QueryRunner } from 'typeorm';

export class Blacklist1539804114893 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "community_blacklists" ("userId" integer NOT NULL, "communityId" integer NOT NULL, CONSTRAINT "PK_43e9841c9a474179f95993cb6fc" PRIMARY KEY ("userId", "communityId"))`,
    );
    await queryRunner.query(`ALTER TABLE "user" ADD "alias" character varying`);
    await queryRunner.query(
      `ALTER TABLE "user" ADD "location" character varying`,
    );
    await queryRunner.query(`ALTER TABLE "user" ADD "email" character varying`);
    await queryRunner.query(
      `ALTER TABLE "user" ADD "telegram" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "preferredContact" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "community_blacklists" ADD CONSTRAINT "FK_bc54752b70bc0614e068e6c2c93" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "community_blacklists" ADD CONSTRAINT "FK_12e51c3f77e95b7944578002b4c" FOREIGN KEY ("communityId") REFERENCES "community"("id") ON DELETE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "community_blacklists" DROP CONSTRAINT "FK_12e51c3f77e95b7944578002b4c"`,
    );
    await queryRunner.query(
      `ALTER TABLE "community_blacklists" DROP CONSTRAINT "FK_bc54752b70bc0614e068e6c2c93"`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" DROP COLUMN "preferredContact"`,
    );
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "telegram"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "email"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "location"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "alias"`);
    await queryRunner.query(`DROP TABLE "community_blacklists"`);
  }
}
