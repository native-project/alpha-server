import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectAdditionalInfoNullable1545072236747
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "project" ALTER COLUMN "additionalInfo" DROP NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "project" ALTER COLUMN "additionalInfo" SET NOT NULL`,
    );
  }
}
