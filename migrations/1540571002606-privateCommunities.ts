import { MigrationInterface, QueryRunner } from 'typeorm';

export class privateCommunities1540571002606 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "community" ADD "isPrivate" boolean DEFAULT false`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "community" DROP COLUMN "isPrivate"`);
  }
}
