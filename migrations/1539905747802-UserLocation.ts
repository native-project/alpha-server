import { MigrationInterface, QueryRunner } from 'typeorm';

export class UserLocation1539905747802 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "location"`);
    await queryRunner.query(
      `ALTER TABLE "user" ADD "country" character varying`,
    );
    await queryRunner.query(`ALTER TABLE "user" ADD "state" character varying`);
    await queryRunner.query(`ALTER TABLE "user" ADD "city" character varying`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "city"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "state"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "country"`);
    await queryRunner.query(
      `ALTER TABLE "user" ADD "location" character varying`,
    );
  }
}
