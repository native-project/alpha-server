import { MigrationInterface, QueryRunner } from 'typeorm';

export class RemoveKYC1553122194360 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "kycStatus"`);
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "kycApplicantId"`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "user" ADD "kycApplicantId" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "user" ADD "kycStatus" character varying`,
    );
  }
}
