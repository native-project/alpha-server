import { MigrationInterface, QueryRunner } from 'typeorm';

export class curatorEmail1540244794042 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "community" ADD "curatorEmail" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "community" DROP COLUMN "curatorEmail"`,
    );
  }
}
