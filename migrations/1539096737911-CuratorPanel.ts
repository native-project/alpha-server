import { MigrationInterface, QueryRunner } from 'typeorm';

export class CuratorPanel1539096737911 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "poll" ALTER COLUMN "description" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "poll" ALTER COLUMN "description" SET DEFAULT ''`,
    );
    await queryRunner.query(
      `ALTER TABLE "poll" ALTER COLUMN "fileUrl" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "poll" ALTER COLUMN "fileUrl" SET DEFAULT 'null'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "poll" ALTER COLUMN "fileUrl" DROP DEFAULT`,
    );
    await queryRunner.query(
      `ALTER TABLE "poll" ALTER COLUMN "fileUrl" SET NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "poll" ALTER COLUMN "description" DROP DEFAULT`,
    );
    await queryRunner.query(
      `ALTER TABLE "poll" ALTER COLUMN "description" SET NOT NULL`,
    );
  }
}
