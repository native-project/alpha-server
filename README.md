# native-server

## Description

Tokenized Community Economies

## Setup and installation

1.  Clone this repo
2.  Install dependencies
    ```bash
    $ yarn
    ```
3.  Copy sample .env.sample to .env
    ```bash
    $ cp .env.sample .env
    ```
4. Copy sample ormconfig.json.sample to ormconfig.json
    ```bash
    $ cp ormconfig.json.sample ormconfig.json
    ```
5.  Make any necessary environment variable changes

## Running the app

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Migrations

```bash
# create migrations
$ yarn migration:generate <name of migration>
#
# run all migrations that have not yet been run
$ yarn migration:run
```

Check the auto-generated migration file. It should be linted and formatted on save if your IDE is set up to follow the Code Quality guidelines below. If it's not already, make sure to capitalize the first letter of the migration class and fix any linting errors that are not handled by auto-fix.  Migrations run are saved to the `migrations` table in the database. [TypeORM Migrations](https://github.com/typeorm/typeorm/blob/master/docs/migrations.md).

## Blockchain Scanning

```bash
# create migrations
$ yarn start:cron
```

While the cron is running, the blockchain scanner will continuously compare the last safe block to the number of the last block scanned for events.  Set the confirmation tolerance in the `.env` file as `CONFIRMATION_TOLERANCE`. The Native Community contracts all emit events through the Logger smart contract.  Set this address to watch in the `.env` file as `LOGGER_CONTRACT_ADDRESS`. If known events are found during scanning, actions can be triggered by registering them in .  

## Code Quality

This project uses git pre-commit hooks via [Husky](https://github.com/typicode/husky) to check code quality. If a commit fails, tslint errors should be the first offender to check. On commit, [Prettier](https://github.com/prettier/prettier) is run first, which will opinionatedly format code against the .prettierrc config, rewriting files and re-staging them as needed. After style rules are applied, [tslint](https://github.com/palantir/tslint) will check for any errors against the .tslint.json config. Tslint --fix **is not** run for errors; errors should be manually fixed. To reduce the risk failed commits, tslint should be run before checking in code. Run linting rules on the entire project: `yarn lint`, or style formatting: `yarn format:all`. Set up auto-linting and prettier to be run on file save or in real-time in your IDE: [WebStorm](https://prettier.io/docs/en/webstorm.html) || [VSCode](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode).
